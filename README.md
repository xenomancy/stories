# Xenomancy Stories

This repository serves as the central hub for all **narrative content** set within the Xenomancy universe, acting as the definitive source of truth for the Xenomancy narrative and story.

While various stories featuring the Xenomancy project might exist elsewhere, this repository acts as the primary source.
Content found on other platforms may be considered derivative works, and any deviations from the stories presented here are not considered canon unless explicitly reflected within this repository.

**For in-depth world-building details and background lore, please refer to the dedicated [Xenomancy Lore Repository](https://gitlab.com/xenomancy/lores).**

This repository is your one-stop shop for delving into the Xenomancy universe:

## Short Stories

Dive into bite-sized tales that explore various corners of the Xenomancy world.

1. [Four of Them](shorts/four-of-them.md). It is a romance short story about communication, featuring four characters from the Xenomancy universe.
2. [Kunjungan](shorts/kunjungan.md). It is a novel highlighting interaction between human kids and aliens from an ice world. Language is Indonesia.
3. [Smartphonology](shorts/smartphonology.md). It is an educational story about how to properly use technology in emergencies. The indonesian version is available as [Smartfonologi](shorts/smartfonologi.md).

## Books

Embark on epic journeys through the Xenomancy saga.

### Xenomancy: We are sorry for the inconveniences

Currently, we are still developing the first book: `Xenomancy: We are sorry for the inconveniences`.
Below is a list of semifinal drafts of chapters:

| Chapter | Title                                                          |
| ------: | :------------------------------------------------------------- |
|       1 | [**Polar Opposites**](xe-1/Polar-Opposites.md)                 |
|       2 | [**The Boy from The Woods**](xe-1/The-Boy-From-The-Woods.md)   |
|       3 | [**Incursion**](xe-1/Incursion.md)                             |
|       4 | [**The Silent Concert**](xe-1/The-Silent-Concert.md)           |
|       5 | [**The ORCA-strated Event**](xe-1/The-ORCA-strated-Event.md)   |
|       6 | [**The Esoteric Business**](xe-1/The-Esoteric-Business.md)     |
|       7 | [**Overtime**](xe-1/Overtime.md)                               |
|       8 | [**Facing The Forking Path**](xe-1/Facing-The-Forking-Path.md) |
|       9 | [**The Lost Son**](xe-1/The-Lost-Son.md)                       |
|      10 | [**Close Encounter**](xe-1/Close-Encounter.md)                 |
|      11 | [Good Heavens](xe-1/good-heavens.md)                           |

### Xenomancy Book 2

Will explore the next stage of the story, where the main casts are exploring an alternative version of Earth to search for Toru El.

### Xenomancy Book 3

Will explore the last stage of the entire Xenomancy arc.
The main focus would be attempts by main casts to fight Powers on Earth.
