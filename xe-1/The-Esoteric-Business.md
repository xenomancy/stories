# The Esoteric Business

> The room for the *Very Important Person* was a mess, with shattered viewing windows, remnants of sword slashes everywhere.
> Security guards laid motionless on the floor, blood sprayed to almost every corner.
> Seated at the VIP couch, was the City Major.
> An open wound was visible on his stomach, he panted, coughed, and was very weak.

## You owe us an explanation

The VIP room must have been breached when the tengus managed to escape the fire veil Xiangyu set up.
Hendrik suspected it was because the security forces opened fire for a few tengus that managed to enter that room.
Non-enhanced bullets wouldn't do anything but to piss them up.

The reflexium particles all around the room started to ping again.
The pings were relayed to his reflexius clusters on Hendrik's right brain hemisphere.
The processed stream of information was relayed to his parietal lobe, integrating the stream into his spatial awareness.
Included in the stream was information regarding open wounds of the Major: damages to his major arteries, weakened heartbeats, and the partially clogged airways.

"My extrasensory returned," said Hendrik.

"Romanov! Call him to save the Major," said Heinrich.

Hendrik nodded, "Romanov Dexter, as a part of the Trinity Compact, I command you to appear."

A spherical distortion came with ocean blue glow.
Two dolphins emerged from it, along with splashes of water that dried off before it reached the floor.
The two dolphins floated about and swam around as if the room was underwater.

One of the dolphins turned into a humanoid figure.
He looked like Hendrik and Heinrich, but instead had a tattoo on the left side of his face that consisted of circles by the temple, and stripes extended from the largest circular pattern to his forehead and his jawline.

"Manov here," he said, his attention was drawn to the Major, "oh, sure."

He put his palm on the wound, and the blood was withdrawn to the wound, while the wound itself started to seal itself.
"Purpose, can you check on the fallen guards?"

The dolphin chirped, and it swam to the fallen guards.
It poked one them with its nose, and the blood was withdrawn back to his wounds.
It poked more guards, and their blood retreated back to their respective bodies.
Except for some bodies, that somehow didn't respond to Purpose's touch.

"Three are no longer alive," a voiceover was heard, superimposed on Purpose's chirp.

"What happened," said the Major as he regained consciousness.

"We mean no harm," Heinrich approached the Major, "there was an attack from the Paramud-"

"-Spirit realm," Hendrik cut on Heinrich's words, "yeah, an attack from the spirit realm."

Hendrik produced a contact card.
It was matte black, with an insignia of a letter "W" and letter "M" stacked vertically.
The width was stretched out, and the Major wasn't sure what to make of the symbol.

"We're the Watchtower Foundation," said Hendrik.
He almost mentioned the abbreviated name, *WTF*, but he realized it meant something else for the humans.

The keyword "watchtower" was enough for the Major to realize that the insignia somewhat appear like a watchtower beaming lights to the sides.

"An esoteric organization, one of our aims was to ensure no supranatural phenomenon was experienced by *feral*, uh, normal humans," continued Hendrik, "consider that we owe you one.
Feel free to contact us with that card, in case your city needs anything.
I believe we can arrange something in return to our failure today."

"Okay, those that can be restored have been restored," said Manov, he then pointed at the three bodies still covered in blood, "unfortunately those three, they're lost forever.
Don't forget to inform their family, they died honorably."

Purpose chirped, and the ocean blue spherical gate opened again.

"We have other business to attend, Paramundus Jagatpadang needs our help," said Manov.
Purpose and Manov jumped to the gate, which then collapsed into nothingness.

"We are sorry for the inconveniences," said Heinrich, he raised his fedora hat.

As they walked on the corridor, Heinrich approached Hendrik,
"What is wrong with just telling him that it was an attack from the Paramundi Complex?
Why must you refer to it as the *spirit realm*"

"Do they need to know technical details," said Hendrik, he turned to Heinrich, "about the virtual cosmology of the RFL Network?"

"No."

"Is there any reason for us to explain more than just *an attack from the spirit realm*?"

Heinrich thought for a moment before answering, "No."

"There you have it, the answer," Hendrik turned away, continuing his pace, and stopped, "oh, a group of people is approaching."

Derictor, Bright, and Nicolas were panting and rested to the side of the corridor when they saw a group of men.
Two men with black cassocks and fedora hats, and a bunch of people with tuxedos, all with fedora hats.
Along with them was a polar bear.
Derictor recognized one of the men in tux as the same man that impaled the denim man with metal bars from the previous concert.

"I believe, hhh-" said Derictor, collecting his breaths, "you owe us an explanation."

Four shadows appeared behind Hendrik, and with a wave of his finger, three of them flew toward Derictor, Bright, and Nicolas.
They could not move, as the shadows crawled on their skin.
Against their will, their bodies walked to the side of the corridor, unblocking the path for Ezekiel, the men in tux, Heinrich, Hendrik, and Xiangyu.

As Heinrich walked past Derictor, Heinrich dropped a glance.
A spare WTF contact card was present in Heinrich's pocket.
He pulled the card and slipped it to Derictor's suit.

"Yes, perhaps we owe you an explanation," said Heinrich.

It was a full ten minutes before the shadow dissipated.
Derictor, Bright, and Nicolas collapsed to the ground.

"What was that?" Bright said, he helped Nicolas to stand up.

"I thought having a shapeshifter as our team mate was the weirdest thing that could happen in our entire operations," said Nicolas.

Derictor knew two half-aliens from his university years: Alex and Steven.
Alex became the CEO of the Department of Treasury (DoT) of the Intelligence Agency, and a founder of the Law Office of Pontirijaris and Associates, while Steven opened a clinic with Nicolas and Derictor's cousin.
They looked human enough, that Nicolas never realize that they were half-aliens.

Derictor didn't believe that they were half-aliens either, until he met Anderson, a true alien.
It was an once in a lifetime experience, thought Derictor back then.
Well, it *was*.
Derictor was certain that night, that it wouldn't be an once in a lifetime experience.

## Doing fine, sans the embarrassment

At the concert room, people were still talking about the fight.
They were enthusiastic, but sad to know that all of their phones were not functional during the show.
Plenty agreed that it was the best concert they've ever attended.

Nurhayati didn't care about any of that, she was too weak to think of anything.
It wasn't a pleasant experience to be electrocuted.
She urinated and defecated the moment she was electrocuted, as the shot disturbs her normal muscle functions.

"Well, at least you don't get a second degree burn on your chest," said Andre, "this will leave a scar."

"Don't mind about the scar, my nephew Doctor Steven will help you with it," said Anderson.

Ernie came to the infirmary, and she went straight to Nurhayati.

"Oh dear, how are you feeling?"

"I am doing fine, sans the embarrassment," said Nurhayati, she let out a smile.

"Don't be embarrassed, you were electrocuted, what else do you expect to happen?
Here, I brought you some juice and oranges, eat them okay?"
Ernie wiped Nurhayati's sweats, and checked her temperature.

"Thank you Ernie, you're the best," said Nurhayati, "is everyone okay?"

"Yes yes, they are.
Scared, yes, but they're doing great!
The dancers were terrified, but they decided to continue the dance, how professional!
I am proud of Arwin, even though his son Dominic died a month ago, he could still sing under pressure!
You wouldn't believe what he said.
He said that he had lost his son, he had nothing else to lose, but not with his voice!
What a gentleman!"

"What about Anthony, was he here?" Asked Anderson.

"Oh Anthony, no he wasn't here.
Tomorrow he had a quiz, so Aditya prohibited him from joining the concert.
They're at their apartment on the East Coast.
Anthony is lucky, his father loved him so much he was tutored directly by Aditya!"

Ernie continued to speak.
Nurhayati was amazed by Ernie's production of an endless stream of words.
Anderson nodded at Andre, that typed on his phone.

> Houston, Info from The Bunny.
> Both of our targets live in an apartment on the East Coast.
> The boy is enrolled in a school.
> Tomorrow he's going to have a quiz.

"This is it," said Kang Hae-In, "finally a clue on Aditya and Anthony."

"What about the mysterious group of psychics?" Said Chandra.

"We don't know yet, but Derictor reported that one of them handed us a contact card."

"A what?"

## My psychic partner

"We're practically defenseless against attacks like these," said Chandra to other CEOs.

The other CEOs murmured as video feeds from the task force were displayed at the meeting screen.
A polar bear that turned fiery, the tengus, the barongs, the living shadows, and a man that could produce a stream of fire.
As if it wasn't enough, from Nicolas's feed, it was shown that the bullets went through the body of the tengu as if it was a mere mist.
Another feed showed when Nurhayati was going to pull a tengu's sword off the ground, it turned into dust, and rematerialized on the tengu's hand again.

"So, what is your suggestion, *Khun* Chandra?" Asked Henokh.

"The group of psychics here appeared to be benevolent," said Chandra, he turned to the footage again, and highlighted the dancing fiery polar bear, "they helped to contain the attack of the spirits, as we can see from the footage."

"So, if we couldn't control the esoteric powers, we should be aligned with one of them," said Henokh.

"Yes, yes that!" Chandra pulled up the matte black contact card, "back then in the UN Peacekeepers, we also employed the help of psychics in some operations, especially if the insurgents used esoteric means of attack.
At least now, we know one of such groups."

"So how do we contact them? Is there an address there?"

"Just a phrase, I suppose it is some sort of spell," Chandra turned the card around, and read the printed letters, "*Fiat nuntium*."

The card turned white.
The other CEOs spectated at the card.
About five minutes, and nothing happened.

"So, where are they?" Asked Henokh.

A beam of light appeared, either to or from the sky, in the middle of their room.
The ceilings, along with the roofs, were disintegrated, and two figures landed in the middle of the light beams.
As the beam dissipated, the roofs and the ceilings were restored, as if it had never disintegrated.

"You," said Chandra, "*Nong* Mike."

*Nong* is a proper way in Thai to address anyone who is younger, usually translated as younger brother or sister.
For those that are older, *Phi* (read: Phee) is used, that literally translated as older brother or sister.

"*Sawatdee Phi*," replied one of the figures, as he bowed with his hands pressed together in a praying-like manner.
It was a Thai greeting referred to as the *wai*, related to the Indian *namaste* gesture.
*Sawatdee* is derived from the sanskrit *svasti*, meaning well-being, and is generally used by the Thai people in place of hello, greetings, and goodbye.

Beside him, Hendrik wasn't sure about what his partner was doing.
To the best of his knowledge, it was a similar gesture to *sembah* in the royal court of Java.
In Bali, the greeting word *om swastiastu* is spoken during the *sembah.*
It was also rooted from the sanskrit word *svasti,* so he reasoned that it was equivalent to *sawatdee.*
In response, Hendrik followed the gesture and bowed with his hands pressed together toward the spectators, while contemplating whether or not he would also say *sawatdee* or *om swastiastu.*
He did not say a word.

"*Sawatdee*," replied Chandra, he turned to the rest of the CEOs.
"Allow me to introduce Michael Carmichael, my psychic partner from the UN Peacekeepers."

"We are not *psychics*!" Protested Hendrik.
Carmichael patted at Hendrik, he shook his head as Hendrik turned at him.

## Different purposes

"Let *Phi* treat *Nong* Mike a cup of coffee," said Chandra, he handed two cups of coffee for Carmichael and Hendrik.

"Aw, *khobkun mak na Phi* (thank you very much Bro)" said Carmichael.

"*Mai pen rai* (it is okay), I haven't seen you for years already," Chandra bought two more cups of coffee for himself and Kang Hae-In.

"Michael *Hyung*, where have you been?" asked Kang Hae-In.

"To a lot of places," Carmichael said in Korean, as he sipped the coffee.

Hendrik looked alternately at Kang Hae-In, Carmichael and Chandra,
When it was between Carmichael and Chandra, they were speaking in Thai.
When Kang Hae-In entered the conversation, Carmichael switched to Korean.
Never did Hendrik know that Carmichael could speak more than English, Indonesian, and Polar Bear.

Carmichael noticed Hendrik's confused face.
He switched to English, "then I joined this *WTF*-"

"The Watchtower Foundation," corrected Hendrik.

"Yes that. I've missed you guys, didn't realize that *Phi* is the one that summoned me in the end."

"Oh, I recognized you," said Kang Hae-In in English, he pointed at Hendrik, "you were the one that petrify Derictor!"

"Yes I was, have we ever met?"

"Well, you meet my squad members," said Kang Hae-In, a delight could be seen in his gaze, "and what they saw, I saw them as well."

"Oh," Hendrik straightened his back, "you're the man behind the cameras."

"We have plenty of time to get to know each other," said Chandra.

Hendrik and Kang Hae-In turned to Chandra.
Chandra turned to Carmichael.
Hendrik turned to Carmichael as well, "what did he mean, master?"

"I agreed with Henokh that we would incorporate about a hundred strong reflexiors to join his organization," said Carmichael.

Reflexiors refer to individuals with the ability to alter their environment through RFL Network.

"Again? We just assigned a number of reflexiors to the ORCA last month," said Hendrik.

"Different purposes.
To ORCA, we were there to guide them and enforce the rules of the WTF.
To this Intelligence Agency, we were after the same party, EPL."

"But, I thought ORCA is currently in conflict with EPL?"

"ORCA is in a defensive role, they had a completely different agenda.
They are uplifting feral humans to achieve *true personhood.*"

"While our organization aims to ensure peace and order, a direct opposition to the EPL's approach of chaos," said Chandra.

"Wait, I'm still here guys," said Kang Hae-In, "what do you mean by sending, *revel-*, uh, *mages* to our organization?"

"WE ARE NOT M-" Hendrik was about to finish it when Carmichael signed Hendrik to stop.

"What else do you expect?
They'd join our ranks.
First, a new department is instituted, the Department of Esotericism, or DoE."

"Michael *Hyung* will be the chief?" Asked Kang Hae-In in Korean.

"No, *Nong* Mich would be the Head of the fifth division," said Chandra in Thai, then he switched to English, "Esoteric Counter-Response, an umbrella division for all combat oriented *psychics*."

Hendrik twitched his eyebrows.
He wasn't sure if it was because of the language switching, or that Chandra used the word *psychics* to describe reflexiors.
Probably the latter, thought Hendrik.

"Oh, yes, Hendrik," said Carmichael, enough to prevent Hendrik from saying anything,
"you, and your two soul fragments, Manov and Heinrich, will join his task force," Carmichael pointed at Kang Hae-In.

Kang Hae-In looked at Carmichael, and turned to Chandra, "the Mobile Task Force Psi seventy-two?"

Carmichael and Chandra looked at each other.
Both turned to Kang Hae-In, and Chandra nodded.
Kang Hae-In couldn't believe it, he would be a fellow division head with Michael *Hyung*.
They would have plenty of time to catch up.

Hendrik couldn't believe it.
He would be in the same squad that Xiangyu wounded to get rid of the camera, and the three guys that he temporarily petrified for obstructing his business.
It wasn't a particularly nice first impression, and Hendrik had no idea how to face them.
