# Polar Opposites

Yam's bluish white stripes where his eyes would be, followed the incoming Seraphim of Hadad.
His serpentine head hissed as he spotted Hadad, leading a swarm of golden Seraphim.
The dark ocean blue scales of Yam emanated bluish white aura as his fists clenched.
His white-scaled chests glowed even brighter.
The great first pair of featureless translucent membranous wings of him spread wide, slowing his flight.
The second pair of smaller wings twitched, just enough for him to adjust his course midair.

His countless number of Seraphim followed his course correction.
Their arms unfolded into thin radiating petals, bloomed into black thorny flowers.
They were aiming at the swarm of Hadad's Seraphim.

Hadad had his pitch black pearly gaze sternly aimed at Yam.
The orb embedded in his forehead sparked blue arcs to his horns atop his golden bull head.
The gold-hued skin of him glowed brighter, and his gold-ish white feathered first pair of wings thrusted hard.
His second pair of wings adjusted his course, as his fists clenched.
He knew the dark blue swarm of Yam would strike hard, his gaze didn't waver anyway.
His Seraphim didn't waver either.

The black thorny flowers flashed bright green beams.
Hadad's Seraphim had their bodies shattered, their wings blazed.
One by one, a number of golden Seraphim fell to the ground.

Hadad waved one of his arms toward a platoon of his Seraphim.
The platoon had their forehead orbs arced bright blue.
Arcs after arcs grew between their orbs and their horns.

In no time, a large number of blue electric arcs smote the ocean blue swarm of Yam's Seraphim.
Yam's Seraphim had their limbs shattered, their bodies shattered, their wings torn.
Another number of ocean blue Seraphim fell to the ground.

Yam's first pair of wings folded close to his body, as the second pair of wings turned hard.
He took a dive, and his Seraphim followed suit.
He aimed toward the ocean, at the time in the state of turmoil.

Gargantuan serpentine bodies danced around in the ocean as he approached.
Some jumped out of water at great force.
Roars could be heard miles away, even by the spectating inhabitants on the shore.

Hadad and his Seraphim chased the descending swarm of Yam's Seraphim.
Jolts of blue arcs from his golden Seraphim smote more and more of ocean blue Seraphim.
Green beams flashed from the ocean blue Seraphim, and more golden Seraphim set ablaze.

## The servant of gods

"It was just a thunderstorm," an elder man said as he was spectating the event by the shore. His voice quivered.

"No, they are Yam and Hadad," said a foreign man.

"They are, who?"

"Yam-Nahar and Baal-Hadad, sons of Bull-El. They are fighting."

"How would you know?"

"Baal-Hadad, is the one that gave me this mark," he said as he uncovered his cowl.
Every strand of hair on his beige skin was silvery white.
His grayish irises gazed sternly at the forming crowd around him.

Not so long after, a number of ocean blue Seraphim and golden Seraphim fell along the shore, and the ocean.
Some crawled to the land, their limbs missing, their wings torn.
They were gigantic, each were three times as tall as the tallest man.

The spectators gazed in awe.
Something about those fallen giants amazed them.
Perhaps it was their beauty, or their stature, or maybe the divinity.

An ocean blue Seraphim rose, amidst the damage on his wings.
His right arm bore a broken black thorny flower.
Its petals shattered, cripled, bent.
He disengaged the petals, and his sleek, serpentine head gazed around.

His body was covered in scales, glowing dim ocean blue aura.
As he turned to the spectators, one could observe the white-scaled chests.
Three pairs of broad chests.
The first pair connected to his arms.
The second, largest and thickest pair connected firmly to his first pair of wings.
The third, smallest pair, close to his pelvis, connected to his second pair of smaller wings.

"Beautiful," exclaimed a young girl.

"Magnificient," said another man.

"What are they," asked the elderly.

"Seraphim," said the foreign man, "the servant of gods."

A golden Seraph rose, he approached the standing ocean blue Seraphim.
His gold-ish white feathered wings stretched, aroused.
The feathers were untidy, some were burnt, and his upper left wing had almost all of their feathers gone.
The body was that of a human, but with three pairs of chests, just like the ocean blue Seraphim.
The skin was almost like a human, except the gold-ish hint and glow.

The golden bull head huffed, his pearly black gaze sternly affixed at the ocean blue Seraphim.
The bluish white stripes met the pearly black pair eyes.
They were gazing at each other, and the ocean blue Seraph hissed at the golden Seraph.

As the orb in between the horns of the golden Seraph arced, the ocean blue Seraph jumped and tore the orb away.
The golden Seraph howled like a slaughtered bull, his hands reached for the hole on his head.
The serpentine head gaped, with a hiss almost like an incoming gust of wind whirl.

Sparks aroused from the fists of the golden Seraph, and they flew toward the ocean blue Seraph.
The ocean blue Seraph shifted into a stance, and the ocean advanced toward them.
The inhabitants ran away, avoiding the approaching wave.
The wave surrounded the ocean blue Seraph, and absorbed the sparks of the golden Seraph.
The ocean blue Seraph, fully submerged, raised an arm.
A very strong, narrow jet stream of ocean water split the body of the golden Seraph into halves.

“Why are they fighting?” The elderly man asked.
The foreign man gazed at the elderly, but gave no answer.

## A snake made of light

He could not chase Yam who dived into the ocean.
Hadad soared just above the ocean, he wouldn't risk diving into the ocean.
Not with the Chaos Beasts guarding it.

Hadad was not at the dead end, as he produced a pair of gauntlets.
One to his left hand, another to his right hand.
He clenched the right-hand gauntlet.

"Yagrush, spread and chase Yam!"

Ribbons of gold lights spread from the gauntlet.
By the end of each ribbon, were wolves made of light.
They scoured, flew, turned, and dived.

The Chaos Beasts attempted to sever the ribbons, but their tentacles were severed instead.
The ocean blue Seraphim attempted to break the ribbons, but the light wolves mauled them instead.
More and more ribbons pierced through the ocean, as more Chaos Beasts and ocean blue Seraphim gathered to stop them.

The ribbons tightened, and pulled up at such a great force.
Yam resurfaced by force, hanging under the might of Yagrush.
A ribbon pierced through his chest, and warped around his body.

"There you are," said Hadad.

Hadad giggled at Yagrush, "it really works."

The bluish white stripes where Yam's eyes should be, winced at the sight of Hadad's gauntlets.

"That, the weapon Kothar made."

"You recognized it, even better!"

Yam said nothing.

If a bull could smile, that would be Hadad's expression at the time.
His huffs sounded like how a bull laughs, if they could laugh.
Hadad directed his grasp to Yam.

"Aymuri, castrate his will!"

Strings emerged, jolted out from the gauntlet.
They danced toward Yam's body, and strapped it tight.
Their ends pierced through every orifice of Yam's body, to the space between his scales, and to every open wound.

Golden hues spread in Yam's body, replacing his bluish hues, inch by inch.
If a snake could roar, that would be the sound Yam made at the time.
His body shook, winced, jolted.

A snake made of light emerged from the body of Yam, constrained by the strings of Aymuri.
The snake wiggled to break free, but the strings tied it harder and firmer.
The snake was then engulfed by the strings, forming a cocoon, into what appears to be a ball of light strings.
The ball of strings was held firmly on Hadad's left hand.
The strings detached from Yam's dimmed body, that fell to the sea.

Hadad smiled.

"Tiamat, is in my grasp now," he said.

## Never give it to Yam

Hadad landed by the base of a cliff near the shore.
Spectators curiously surrounded him, and were surprised by Hadad's size.
Hadad was almost twice as tall as his Seraphim.
He recognized one man among the crowd, a foreign man.

"Alt Parahomen, the killer of your own brother, Has Homen, I did not expect to see you here."

"Lord Hadad," praised Alt.

Alt's fists clenched.

"How long has it been, nine thousand years?" Hadad bursted a giggle.

Alt's teeth grinded.

"It appears Has's blood keeps you alive for this long! Oh poor Alt," Hadad's giggle evolved into a laugh.

Hadad was still laughing, when a cliff gaped wide, and black, featureless, furless wolves with glowing white eyes emerged from the opening.
The wolves were unusual, as their forelimbs had unnaturally long and slender fingers folded to their humerus.
Those long, slender fingers were connected with some sort of membranes, resembling those of bat's wings.
The membranes connecting their fingers also connect to the side of their body, and to the base of their pelvis.
Another set of membranes connect the behind of their rear limbs with their thin, furless tail.

A giant, wrinkled, and ancient wolf with a tall, raised pair of ears followed them.
Its wing membranes were full of holes and damages.
Its eye-stripes were at level to Hadad's pearly black eyes.

"Mot," said Hadad.

The ancient wolf nodded, and opened its mouth.
It spoke in a surprisingly human-like voice.

"What is it?"

It didn't even sound like Mot was asking.
The voice was flat, featureless, almost without a soul.

Hadad spread his hands and wings.

"*What is it?* You're asking me 'what is it?' Is there nothing you want to say to your nephew?" Hadad raised an eyebrow, if a bull had one.

"What is it?"

Hadad's smile wore out, his lips tightened.
He folded his arms, and said, "I am now the rightful heir of the Divine Council. Don't you want to say anything about it?"

"What is it?"

There was a silence.

Alt burst in laughter.
It tickled his mind that the so-called mighty Hadad wasn't even respected by his own uncle, Mot, the God of the Underworld.
*It was worth watching,* Alt thought to himself.
He stopped just in time, as Hadad gazed toward him.
At this point, Alt had to also consider not to offend him too much, he didn't wish to have any worse punishment than what he already had: immortality.
It was good enough that Has' blood made him youthful and immortal at the same time.
The last thing Alt would want to have would be immortality without perpetual youthfulness.

Hadad gazed back at Mot, "Forget it. I just want you to keep this with you."

Hadad handed the ball of light string to Mot.
Mot opened its mouth and swallowed the ball.

"Never give it to Yam," said Hadad.

Mot said nothing, and returned to the opening.
The black wolves followed it to the opening, and the opening closed.
The cliff returned to the way it was, no sign of a hole at all.

There was another silence.

"That's it? *He* is certainly not amusing," remarked Hadad.

He spread his wings, and faced the crowd.
He spread his arms, as if warning the crowd to stay away from him.
The crowd gave him some space.

He jumped, high, and he glided up.

His Seraphim already flew toward a giant hanging ark, advancing west-ward at great speed.
He catched up with the ark, and the ark was gone over the horizon.
The spectators can only gaze around, to discover that all of the Seraph bodies had been cleaned up, and the ocean was calm.

## Make it right

"And that's all?"

Aditya was facing Henokh right beside him.
His elbow supported his head and shoulder.

"Yes, that's all."

Henokh didn't change his position, lying comfortably on his pillow.

"The story that your grandpa always tells you?"

"To the best that I can remember, yes."

"So, what does it mean?"

"Just a story," Henokh giggled.

Aditya laid back to the carpet they spread on the concrete floor, gazing at the stars.
There wasn't much to see, as light pollution of the city hid a significant fraction of the stars.
It was still the best location they favored for their weekly meetings.

"Your grandpa knew a lot of good stories," commented Derictor, right beside Henokh.

"Definitely much better than what Steven always tells to Xiangyi, right?"

"The ones about extrasolar gods? Your story wouldn't compare to his," said Derictor.

Henokh didn't say anything.
His lips tightened.
Derictor peeked at Henokh, and giggled.
Aditya followed giggling.

"No, really. But this one, your story, is more down to Earth, you know," said Derictor.

Henokh couldn't help it, but he stole a gaze at Derictor.

"El, Yam, Hadad, Mot, they're patron gods of the Canaanites. The way they're described in the myth reflects the cultural growths of our society."

"How so?"

Henokh couldn't help it.
He had to ask.
A grin started to form on his face.

"Yam is an important deity for the Phoenicians, a seafaring civilization. For example, the tripartite division of the Phoenician religion, Baal, Mot, and Yam, is thought to be a direct influence to Greek division between Zeus, Hades, and Poseidon."

Henokh nodded.
His gaze returned to the stars.
His thoughts wandered around.

"One thing I couldn't understand is, why Baal-Hadad and Yam-Nahar must be fighting?"

Derictor was about to speak, but Aditya spoke first.

"Yam, despite being portrayed as the god of the sea, is actually representing chaos, as opposed to Hadad that is representing order."

"Yes, yes that," said Derictor, "the fact that Hadad won in the story, represents our communal desire to adopt order. Order leads to civilization."

Henokh nodded, "so where is that order now? With all of the corruptions occurring in this world, and ugly politics and everything. It is chaotic."

"We can make it right," said Aditya, "order is inherently unsustainable. Chaos, on the other hand, is the natural order."

Henokh and Derictor turned silent.

"To restore the natural order, we destroy the current civilization, and start anew."

"I think," interrupted Derictor, "that's too extreme."

Henokh nodded.

## Pupils of crocodiles he saw at the zoo

A day after, Henokh happened to sit on a bench beside a tree.
It was two hours before the next class started, and he decided to read a novel.
Little did he know that a man had been gazing at him within the past hours.

The man decided to be seated beside him, and greeted.

"Anderson Pondalissido," he offered a hand.

Without thinking, Henokh greeted the hand, and answered, "Henokh Lisander."

That was when he realized the pale white skin of the man, along with his all-white hair.
He wore a sunshade, and his ears were pointed.
His body was huge, and bulky, but something about him was emanating calmness.

"Grandson of Altair Oldman?"

"Yes," said Henokh.

"I'm from the Securion Incorporated International. Your grandfather was one of the shareholders."

"..., yes?"

"You must be very surprised by now."

"Yes, yes indeed I am."

Anderson fixed his position, and with a stern gaze, said "I am the answer."

"Of?"

"Of change, that you wish to achieve."

"What?"

"Chaos is definitely not the answer to this," Anderson let his answer sink in Henokh, before he continued, "Change must be applied in an orderly manner. And I can provide you that."

He opened his shade.
His eyes were almost all white, except for a vertical slit in the place of his iris.
It was almost shut tight.

"I am an etoan superior, one of the etoan Earth colonization initiative. You must be familiar with this, one of your acquaintances, Steven, Steven Pontirijaris, is also an etoan."

Henokh didn't say anything.
He froze.
It wasn't a fake contact lens or something.
He believed he saw the vertical irises twitched slightly.
It was as real as the vertical slit pupils of crocodiles he saw at the zoo.

"I have the resources, and manpower, required to slowly apply the change, for a betterment of the human race. But I need you to be a part of our team."

## Do your part

A day prior to his meeting with Henokh, Aditya was lying down in the middle of a soccer field, gazing at the stars.
The lights interfere with the sight of stars, that he could almost see no star at all.
He didn't bother, he just pondered about life, the society, and everything.

That was when he saw something flying toward him.
He sprang up, that he strained his stomach.
His heart skipped a beat or two.
He forgot to breathe, and his knees shaked.

He had no idea what he was seeing.
If he were to describe this being, he'd describe it as follows.
In front of him was what Aditya could vaguely describe as a humanoid figure.
His head was like a snake, but instead of a pair of eyes, it got a pair of light stripes.
His scales were blue on the dorsal area of his body, along with his extremities.
On his ventral area were white scales.

He had six pairs of pectoral muscles.
The middle being the biggest that connects to a pair of strong limbs, supporting its serpentine membranous wings.
The uppermost pair connects to his arms, and the lowermost pair, on where it should be abdomen, were connected to a pair of secondary, smaller wings.
It was also tall.
Aditya felt dwarfed compared to it, like he was a child looking up to an adult.

"Be not afraid. You are looking at Lord Yam-Nahar."

Aditya's feet lost their strength, and he collapsed.
Before he knew it, he was kneeling in front of the beast.
Aditya said nothing.

Yam-Nahar kneeled down, and gazed at Aditya.
Aditya gazed back, he couldn't help it.
The white stripes where his eyes should be, have ribbon-like tails that waved around behind his head upwards.

"I have a proposal," Yam-Nahar started.

Aditya said nothing.

Yam-Nahar let out a certain amount of time to pass, and continued, "We both know how the world is right now. People live without guidance, without awareness. It was chaotic. The divines barely intervene. People worried about trivial, unimportant things."

Aditya nodded.

"It wasn't always like this."

Aditya stood still, on his knees.
He was certain that he could see the beast in front of him frowned, if its serpentine face could frown.
The next thing Aditya felt was Yam-Nahar's stern gaze piercing through Aditya's soul, followed by an offer, "If you follow my path, I can make this world reset. But to do that, you have to obtain what was taken away from me."

"H-H-What?" Aditya's voice finally returned.

"My will. My desire. You could also say, my balls, my courage. Whatever suits you."

Aditya's voice was retreating away from his throat again.
All he could voice was an inaudible mumble.
He couldn't even think what to say.

"Tomorrow. You'd listen to what your friend says. Pay attention. You'd know what I mean."

Aditya believed that the beast smiled, an assured smile.
In its eye-stripes, Aditya could sense satisfaction, as if Aditya already did the deeds Yam-Nahar requested.
The satisfaction was just there, as if the being knew Aditya could do it.

Lord Yam-Nahar rose, and turned his back to Aditya.
He walked away.
After a few steps away, he turned to Aditya,

"As of how, I'd guide you. You just have to do what you feel is right."

It was right at the moment Aditya was going to say, "How could I know which one is the right thing to do?"
Aditya hadn't had the chance to stop his mouth from asking it.

"I am Lord Yam-Nahar, the god of the sea, of the primordial chaos. I rule chaos, and patterns. I can make you do the right thing without you knowing. You just have to do your part."

Lord Yam-Nahar gazed at the sky, and jumped.
A strong gust of wind threw Aditya a few meters back.
Lord Yam-Nahar was no more.

Aditya rose and went to the spot where Lord Yam-Nahar last stood.
Giant footprints could be seen.
He touched it.
It was real, the depression was real.
He explored the rim, he explored the base.

He took pictures of it, but rather, he videotaped it.
He was lucky he videotaped it.
The depression bounced back, and the grasses restored in place, their wounds healed.
It was as if there were no depressions at all.
Except that Aditya had it videotaped.

Aditya played the video again and again that night.

## Yesterday I met a guy

After their meeting, Aditya took his time to reconsider everything.
Lord Yam-Nahar didn't joke around.
He did get an answer to his question of what he must take, from Henokh's story.
He didn't know how to do it by himself though.
He needed a partner to do it, and the only one he could think about, would be his best friend, Henokh.

Henokh took his time to think about what Anderson said.
Steven said he knew Anderson, a fellow etoan colonists, a best friend of his grandpa.
When Henokh asked where his grandpa was, Steven said he was currently in a journey of attending various high schools on Earth.
He wouldn't normally believe in Steven's stories before.
It sounded absurd, and naturally he'd consider Steven was a man with big words, big lies.
He used to ponder, whether or not Steven genuinely believed in his lies.

His meeting with Anderson changed everything.
Steven's stories could be true.
Hidden society of foreign hominids, living among men in plain sight.
Steven didn't seem to hide the fact either, but why nobody paid them any attention?

Anderson's proposal, on the other hand, was a sound one.
However, he would need the support of his friends.
Above all, he wished that his best friend would accompany him in this absurd journey.

Henokh, Derictor, and Aditya decided to meet a week after their last meet up.
Henokh wanted to tell his friends about his encounter with this etoan superior.
Aditya wanted to tell his friends about his encounter with this Lord Yam-Nahar.
Derictor wanted to meet with his friends again and to see what they're up to.

"I can't do it myself," said Henokh and Aditya at the same time.

There was a pause as three of them gazed at each other.

"I want to do it together," said both, again.

Derictor gazed at them.

"What happened?" Inquired Derictor.

The two turned silent.
They realized how silly they’d sound.
Aditya thought his experience with Lord Yam-Nahar was too absurd for them.
Henokh thought it was crazy to say that an alien was asking them for help.
No one said anything for some time.

Derictor, irritated, gazed at Henokh, "You first, then."

"So...,"

Henokh lost his words, upon realization of how absurd it is, what he was going to say.

"So?" asked Derictor.

"Um, you know this Steven, right?" Asked Henokh, "that always tells us incredible stories, about extrasolar gods, about his smart car, and his *smartphone*?"

Aditya and Derictor nodded.

"What if," Henokh paused, "he's actually not lying about it?"

Derictor had his eyebrows raised, "..., what?"

Aditya didn't say anything.

"Yesterday I met a guy," Henokh chuckled, realizing that the next sentence would be an utterly ridiculous statement, "saying he's etoan superior. His eyes were like that crocodile eyes we saw at the zoo. Except that his eyes were all white, with vertical slit pupils,"

*Screw it*, Henokh thought to himself.
He'd just spill it out until he ran off something to say.
If they laughed at him, he'd just say it was just a joke and move on.

So he told them about his encounter.
Aditya and Derictor listened through it.
The more Henokh spoke about his weird encounter, the more Aditya realized, his story was at least as crazy as Henokh's story.

After Henokh was done with it, it was Aditya's turn.
He explained that a day before their meeting, he had this weird encounter with Lord Yam-Nahar, and how he said what he wanted to know will be told by his friend the day after.
It corresponds with the day Henokh told them about the story his grandpa always told him.
He also explained that, because of that, he believed the answer lies in Yam-Nahar, and they just need to follow his path.

Derictor was stupefied for a moment.
For two moments.
For three moments.
Derictor could not wrap his head around them.
His two friends turned lunatic in just a week.

"No, you gotta trust me in this," pleaded Henokh, "Anderson is the answer."

"Lord Yam-Nahar is the answer," Aditya added.

"Can we just ignore the answer?" Derictor paused, he almost let out a scream, "it was just a midnight talk, they were all nonsense."

Aditya and Henokh didn't say anything.

"Guys, do you seriously understand what you're talking about? Do you realize how silly they sound?"
Derictor gazed at Henokh, and then Aditya.
No one said anything.
"Henokh? Aditya? I believe I am not talking with walls right now, where are your answers?"

"I believe in what I saw," said Henokh and Aditya in a chorus.

Derictor hated that moment.
Both of his friends didn't lie about what they believe in.
But neither made any sense.

Henokh pulled his smartphone and showed Derictor a short video of Anderson and his eyes.
It was two thousand and eight, and the best camera for phone was around five megapixels.
With his candybar smartphone, equipped with a hundred and twenty eight megabytes of *Random Access Memory*, and internal storage of eighteen gigabytes, he showed them a clear sight of Anderson's eyes in a video of *Video Graphics Array*-compliant resolution, and took several photographs at close up range in five megapixels.

Aditya, another equally rich boy, pulled a similar phone.
He showed them a VGA-compliant video footage he took of Lord Yam-Nahar's footprints, that renormalized shortly after.
Unfortunately he did not take any pictures with his five megapixels rear camera.

Derictor saw both footages, and he was having a headache.
He could not determine which of them were telling the truth.
Both could've told the truth.

Derictor let out a sigh, and shook his head, "I am sorry, I can not choose." Derictor gazed at both of them, he wasn't even sure what face to make, "I think you two should resolve this."

Sufficient to say, that moment was not the best evening in their life.
