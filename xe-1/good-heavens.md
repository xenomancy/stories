# Good Heavens

Darkness dissipated and rays of orange light spread from the east.
The breeze was still cold, and Ostaupixtrilis Pontirijaris knew it would warm up soon.
Waiting for the sun to rise, he set his gaze to the glimmering ocean far on the horizon.
The breathtaking eagle-eye view was one of the privileges he enjoyed from living three kilometers above the sea level.

Sidsido Ahidei is called a Hanging City for a good reason.
It was a highly stratified city, not by social classes, but by its unique structure.
It is comprised of many plates suspended from hundreds of thousands of thick carbon nanotube reinforced threads, connecting a supra-mundane strip habitat more than a thousand kilometers up ahead to the ground.
He knew by his heart that the habitat formed a continuous band encircling the entire planet, aligned to the planet's equator.
He wouldn't have seen the sunrise because of the band, but its underside compensated with an artificial sunrise - a holographic recreation of the light that bathed the area underneath it in a similar glow.

He looked at many other suspended plates beneath him, each covered with its own distinct cityscapes.
He looked up above him, where there were as many other suspended plates, but only their rims were visible by him.
Each plate has their own holographic projectors, creating a realistic illusion of an open sky.
He knew, that from ground level, all of those plates would be mostly invisible, except maybe at the rims, where people can see the demarcation of the holographic projectors and the actual metropolitan landscape atop of each plates.

He didn't pay attention to any of them though, as he was happy with his own estate, three kilometers from the sea level.
Even in the early morning like these, people of various races and sizes were buzzing around the local plaza.
It was the very estate that had been in his possession for almost ten thousand years.
He had gradually acquired over sixty percent of the shares over the millennia.

Ten thousands year ago, when Earth stopped accepting immigration, he knew he had to create a piece of Earth for himself somehow.
Using his power  as a major shareholder, he shaped the estate to his own liking.
His vision came to fruition as people from other plates eventually recognized the estate as the *Earthtown*, with most of its residents either descended from, or actually from Planet Earth.
His own reason on acquiring the Earthtown was a simply longing for his roots - he himself had been raised as a humble Earth man fifteen thousand years ago.

## Thank you, Red and Black!

The sun rose and the sky turned blue, the shade of blue brighten slowly.
He hesitated, the soft gras tempting him to linger, but the rumbling in his stomach reminded him of breakfast.
He felt a hand on his shoulder, stiff and cold as a well polished natural stone.
It was cold, but comforting.
Which tells him the identity of the hand's owner.

`You weren't at the office`,
was all Ostaupixtrilis able to understand from the tight gigahertz radio frequency beam the entity emitted.

`I figured you would be here, gazing at the sunrise.`

The entity proceeded to wrap its arms around Ostaupixtrilis' trunk.
Ostaupixtrilis caressed the entity's hand, as its trunk was pressed against his back.
Artificial warmth could be felt from the entity's statue-like body.
Ostaupixtrilis focused his gaze on the arms of that entity.
It was pitch black, so nonreflective that it looked like a hole in reality with the shape of humanoid arms.

"You know me too well, Adran."

`What is bothering you, might I ask?`

"I am starving of some *pork feet broth*."

The entity was silent for a couple of Ostaupixtrilis's own heartbeat, then it continued,
`You missed Earth, it seems.`

Ostaupixtrilis did not respond, but the entity knew just what he needed.
It carefully detached its hug, rose and offered a hand to Ostaupixtrilis, which complied and stood up.
The smart grasses weaved around Ostaupixtrilis to form a simple short dark green trousers.

Those two figures walked amidst the buzzing city square.
In the past centuries the Earthtown had adopted eastern architectures.
Everyone in Aucafidus knew that humans of Han descent made up a significant portion of Earth's population in the early twentieth first century Anno Domini.
Earthtown, however, existed in Aucafidus, not Earth.
Despite being mostly from Earth, only a fifth were humans.
Another fifth were uplifted dogs, with the rest being a mix of higher porpoises, corvids, elephants,  and other Earth species, making up two-thirds of the population.
The remaining third comprised of mostly Etoans and artificial entities.

A number of dog puppies the size of chihuahuas ran toward them.
Their barks were excited and their tail wiggles.
Unlike their Earth counterparts, their vocalization and body languages encoded grammatical and lexical components on them.
Ostaupixtrilis' own exoself translated them effortlessly into forms legible for him.

*Uncle Ostau! You came!*

"Red! You have grown a lot!"
The puppies were jumping in chaotic turns around Ostaupixtrilis, and he was busy calling each of them by names.
They were Red, Black, Brown, Stripes, Polar, and Orca, the latest batch of the Red and Black clan.

Somehow they were keeping a safe distance from Adran.
Ostaupixtrilis knew it must be something to do with the fact that Adran is completely and thoroughly odorless.
To the puppies, Adran was an enigmatic god, and their attitude to it was more of cautious respect than fear or disgust.
They were not wrong, as the entire estate was managed by Adran with its many pitch black drones.
The drone by Ostaupixtrilis' side was just one of many that kept the estate running smoothly.

Without saying a word the puppies agreed to just focus on their favorite person.

"We missed you, Uncle Ostau! Did you bring treats?"

"Shoot, I did not! But I got something better," he jumped to capture one of them followed with adorable playful screams.
Another second later, he was buried in a mass of fluffy noisy puppies.

On his knees, Ostaupixtrilis was busy on giving them belly rubs and grooming their furs in turns, one by name, while calling their names.
Hearing their names being whispered by Ostaupixtrilis arouse their tail into rigorous wiggles.
One of those excited tails combined with their bounces was a tad bit too much that Black hit the leg of a table.
On the edge was a bowl of shaved ice soaked in syrup, then it was not the only one soaked.

"Aaah!"
Black shrieked as the cold sprawls on his back, sending chills up their spine.

From inside of a restaurant they were disturbing was a yell that turns all of those puppies into attentive statues, or rather, dolls,

*BLACKIE! I swear to god if YOU don't clean up THAT MESS.... -Oh, Ostau!*
The voice that was a roar turned jovial in an abrupt transition.

"Snout!"

The puppies scattered away to a tall and slender figure of a massive Doberman with a lively pair of eyes, and a happy waggling tail.
They were adorned with silky black fur, decorated with patches of reds on its underside.
Their menacing canines were well-cleaned, shining in bright white.
The Doberman called Snout was ready to greet Ostaupixtrilis with a jump.

"I am sorry for the scene-" was all Ostaupixtrilis could say before he was being showered with happy barks and a strong hug from Snout.
Snout's front paws were on Ostaupixtrilis' shoulders, their snout was resisting the urge to lick his face.
They however could not resist the desire to sniff out all of Ostaupixtrilis' facial scent.

"*Don't mention it,*"
Snout whispered in barks, but the barks were carefully modulated to resemble a language Ostaupixtrilis understood.

"*I am just happy to see you here! Please come by, I'll treat you with your favorite!*"

There was a pause between them, then Ostaupixtrilis looked at Adran.

`Well, you are in need of a morning sustenance.`

A smile grew wide on Ostaupixtrilis' face, and he closed the gap between his face and Snout's nozzle.
Snout pressed their face hard on his face, then with great affection and care proceeded to lick his face clean.
Their tongue was warm and soft on Ostaupixtrilis' face, and he reciprocated with gentle carressing of the giant Doberman's head, neck, and chest.

"I'd be more than happy to enjoy your Pork Feet Soup," said Ostaupixtrilis, while immediately being replied with an enthusiastic lick on his nose and eyes.

Snout turned its nozzle to Adran, and while they were not making a sound, Ostaupixtrilis could understand what they were saying at that time,

*If only you can eat, Adran, I'd also offer you a bowl.*

`I'd be happy to also have a bowl of it. I am perfectly capable of processing biont consumables.`

*Perfect! Two bowl of pork feet soup will be ready soon!*

Snout didn't take a pause, they immediately departed to the kitchen.
A Siberian Husky came to them not too long after.

*Hi, I am Bush, and I am terribly sorry for Snout!*
*They were too excited that they forgot to invite you two gentleman to a seat.*
*Here, do sit here, and please make yourself comfortable!*

The restaurant was constructed out of authentic Earth teak with post and beam construction.
Thick teak posts made the structure support along the wall and the corners, then the wall is comprised of teak beams.
At a glance the structure was more than obvious to be not fireproofed.
However large induction cookers on the kitchen were providing the heat necessary to process the food while remaining safe for the naturally heat resistant teak wood.

Thick savory scent of pork broth was omnious in the restaurant, on bowls served by a legion of dog waiters to hungry customers.
There were other native etoan and human customers, and a small group of bear guests seated on communal long tables, one of which was where Ostaupixtrilis and Adran were seated.
For canine and feline guests, there were platforms comprised of wooden floor plans on the side opposite to the communal long tables.
Each level of the platform is too short for hominid guests, but was more than comfortable to cater canine guests and their food bowls.
The food bowls were clustered to cater a group of ten each, that would accomodate a moderately sized pack, with larger packs can occupy a number of food bowl clusters.

The restaurant, "Black, Red, and Litters" was primarily serving canine and hominid guests.
However a significant number of corvids were also well catered on the restaurant's wide ceilings.
There were wooden planks arranged into intersections and in multiple levels on the ceilings.
Corvid guests would gather and chats on the planks, while enjoying diced meats and beans from their respective feeding apparatus.
The corvids usually arrive in large number, and they were almost always gathered together irrespective of clans or familial relationships.
Their chatty and playful nature means there were no apparent segregation of groups for outsiders, as various groups interact with one another.

Since the establishment did not serve seafood and vegetarian options, cetaceans and elephant guests rarely come.
In that particular day, there were only a couple of cetaceans floating by near a group of crows.
They did not appear to consume anything, as they were only accompanying their corvid friends.

Ostaupixtrilis was too busy observing the establishment and its guests, that he did not realize Red and Black have arrived with their wooden cart, containing some tens of porcelain bowls, each covered with their respective and matching lids.

*Uncle Ostau, Sir Adran, your servings are ready!* said Red, with their nose rubbing Ostaupixtrilis' back.

Ostaupixtrilis was too surprised to respond, but Adran already took two bowls from the cart.
With surgical precision and inhuman coordination, Adran put the bowls in front of them, and remove the lids, all in one coordinated beat.

"Thank you, Red and Black!"

*You would not believe it, but Uncle Ostau, you are amazing!* Said Black in a light set of barks.

*Uncle Snout was too happy that they literally forget to punish Black for the broken bowl!* Continued Red in a playful bark.

Ostaupixtrilis was about to give each of them them a pat on their head when another figure came out from behind the cart.

"*Uncle Snout did not forget, they were only suspending the punishment since a special guest is here,*" said Snout in a speech, *they might decide to add more punishments if you don't let Uncle Ostau and Sir Adran enjoy their meal!* Snout continued in a silent glare.

Red and Black hurriedly carry the cart to their next customers, while Ostaupixtrilis was laughing while wiping his tears with a finger.
Adran was silent but observant, and Snout would not remove his gaze from Ostaupixtrilis.
Snout's tail that was stiff in anger then waggles as Ostaupixtrilis turned to them.

"Your restaurant is as busy as always."

Snout jumped from side to side, "*I know, right! I personally oversee the rennovation! Including the addition of accommodations for the corvids!*" They paused for a moment, then cocked their head to the side, then without a vocalization it continued, *Even though it was hard to convince them at first to come to a restaurant run by a family of dogs!*

"But you did manage to convince them," was all Ostaupixtrilis need to say to lighten up their eyes.

"*YES! YES I DID! I am a good boy am I not?*" Their waggles got stronger that their hips were visibly swaying in rhythm, their front paws were already in Ostaupixtrilis' grasp.

"*And you are the one not allowing them to eat, dear!*"
Bush put their left paw on Snout's front paws, indicating an order to put the paws down.

"*Aww, sorry dear,*" Snout's ears were down along with their head, their tail were still waggling but slower.

Bush told them to enjoy the meal as they escorted Snout back to the kitchen, as the day was still early and new orders keep coming for them.

`I believe it is a sign that I made the right decision to bring you here,` said Adran, `you did not stop smiling since we get here.`

Ostaupixtrilis put his gaze on a region where there should be eyes if Adran's drone was a human being, but instead only darkness stared back.
He put his hand on Adran's hand, "Yes Adran. Thank you. I did not know that it is exactly what I needed."

`I suspected so, this place is your anchor to reality when you get lost. Eat, you will need your energy to face the day.`

The pork feet was boiled for hours that the tendons connecting those pork digits become loose and soft.
The skin was thick but soft, his tongue could feel empty follicles on the skin, enriching the texture.
The taste was savory and has a gelatinous texture, that he could not stop sucking the digit bones to get all of the meat off the bones.
The broth, enriched in red beans and pork fat, gently massaged his taste buds.

Adran, meanwhile, methodically disassemble the feet in its mouth cavity bite by bite.
By whatever means its body language appears to signify enjoyment of the taste.
Then it gulped the broth down its mechanical throat, each gulp was made audible.

Ostaupixtrilis was observing Adran eating, "Why do you decide to join me for a meal?"

`A meal is best served with a friend,` it put down the bowl, `I believe it would make the taste better.`

"I haven't seen you eat anything but to replenish your nanogoos, perhaps for many thousands of years. I believe it must be five thousand years ago when we first meet each other."

`A trip to Stratos, yes I remember it,` Adran paused to think, `I was giving Leaf's meal a taste, solenadactilian worms I believe.`

"Don't play dumb, Adran, I know you can effortlessly remember that day with your perfect memory."

`Today is a special occassion,`
`I want to make you as comfortable as possible.`
`Including making you feel like being accompanied with a proper biont.`
`I intend to fool your brain into thinking that I am a real biological entity.`

"So thoughtful as always," Ostaupixtrilis smiled, he could not remove his gaze from Adran and its plain mechanical honesty, "whatever it is, you did a good job. It works."

`For the same reason, I have also taken the liberty of witholding an important information from you.`

Ostaupixtrilis raised an eyebrow, it was extremely rare for Adran to withold information from him, "would you tell me if I ask you what it is?"

`I do not need to disclose it to you. You will know it by yourself soon.`

"What do you mean?"

## An opportunity to taste

A warm and fuzzy pair of arms slithered around Ostaupixtrilis' shoulders, and it wasn't long before an accompanying familiar face pressed against his cheeks.
Her silvery white hair blanketed Ostaupixtrilis' neck, and her breath was a comforting voice he longed for.

"Tudva," Ostaupixtrilis' hand reached for her hands, he caressed her furry long-sleeved black sweater, "when did you arrive to the estate?"

"As soon as *we* received a call from Adran," her whispers tickled his left ear.
His giggles grew as he put her face away from his tickled neck.
But one of her words piqued his interest.

"Wait," his face turned to her, "*We*?"

"I came with Leaf of course."

A set of low pitched guttural grumbles and pungent odors came to Ostaupixtrilis' senses.
It was a sensation far from pleasant but was familiar for Ostaupixtrilis, that his exoself effortlessly translated them into something he could understand.

*We would not miss an opportunity to taste the pork feet soup here.*

Ostaupixtrilis turned to find a green beast standing behind him and Tudva.
The beast stood with a pair of thick legs, partially supported with their third limb that act more like a tail than an actual limb.
They had two pairs of manipulator limbs near the central mass.
The lower pair was much smaller, those thin furry serpentine limbs carried a bowl of pork legs soup with dexterity.
The upper pair was as huge as their rear limbs, each with thick and sturdy manipulator tentacles on the ends.

They use their fore limbs and their "tail" to support its body around the table, and positioned their rear limbs in a seating position.
Their lower manipulator limbs positioned the bowl to the table, then they gingerly opened the lid with the right lower manipulator.
Ostaupixtrilis could see rows of finger-sized tentacles lined up on the underside of each of the beast's limbs, the rows converged on a digestive opening on its chest.

"Oh come on Leaf," Tudva took a seat beside Adran, in front of Leaf, "the amino acids and the sugar molecules are of the opposite chirality as the one your species use. Your body would not be able to metabolize them."

Its right lower manipulator dipped into the hot broth to taste it, but they immediately pulled the tip out accompanied with a deeper guttural scream.
A polar bear the same size as them offered a wipe to Leaf, to which Leaf thanked the polite polar bear's offer.
They shoved the right lower manipulator to the wipe on the polar bear's paws.
The confused polar bear rubbed the tip of the manipulator arm, which when it was thoroughly wiped clean, was pulled back to reach for eating utensils on the table.

*We would still be able to taste it nevertheless, but we are thankful for your concern.*

Ostaupixtrilis, Adran, and Tudva both looked at Leaf as the ten-brained creature carefully tasted the broth with the sensory tentacles on their "head" and the tips of their lower manipulatory limbs.
At the base of each of the beast's limbs was neural ganglia, responsible to control their respective limbs, except for their lower manipulator limbs that has two fused neural ganglia each.
The ten ganglia were connected in a ring brain configuration encircling the central digestive opening.
As the brain encircled their mouth, they have evolved to gain pleasure when objects were shoved through the sensitive digestive opening.
Without saying a single word, Ostaupixtrilis, Adran, and Tudva knew Leaf thoroughly enjoyed the meal.


