# The Boy from The Woods

Aditya found himself wandering in the woods.
It was what he liked to do in his free time.
A fresh start, from everything that had been the part of his life in years.

Friendship he had with Derictor and Henokh wasn't particularly a bad one.
It was just that, after their fight on Anderson and Lord Yam-Nahar, they were drifting away.
That, and the fact that he knew parting ways would be the right thing to do.

Aditya had been wondering about what Lord Yam-Nahar said to him.
The instructions themselves were very vague.
To achieve his dreams, Aditya had to acquire the object that Lord Yam-Nahar said to be his *will*.
Lord Yam-Nahar said he would guide Aditya, but Aditya had no idea how.
*Just have to do what you feel is right*, said Lord Yam-Nahar about his guide.
Aditya shook his head, it was still too abstract, or rather, absurd.

Until he heard a cry in the middle of the woods.
He looked for the source of the crying, penetrating the dense trees of the forest.
That was when he saw it, something like a sign.
A boy alone in the woods, accompanied with a vague floating water blob that somewhat resembled a dolphin.

"*Eomma, Appa,*" cried the boy, "*eodi gyeseyo?*"

The dolphin-like water body floated about, and danced, and chirped, apparently in an attempt to cheer the boy up.
Aditya didn't think that it would work.
The boy screamed louder, and the dolphin-like water body seemed to panic.

"Is this considered *a sign*?" Said Aditya to himself.

The dolphin-like water body noticed Aditya, and it zoomed straight to him.
To Aditya's surprise, the dolphin-like water body introduced itself in plain English,
"I am Aurelia, a friend of this boy.
His parents left him alone in the woods."

"What?"

"*Ahjussi,*" said the boy, "*bumonim-eul chajdolog dowajuseyo.*"

"He asked you to help him find his parents," said Aurelia.

In his cry, the boy hugged Aditya's leg.
The boy was very young, probably around four to five years.
Aditya kneeled down until his eyes were more or less at level with the boy’s.

Aditya asked himself, or to Lord Yam-Nahar, or was he praying?
*Is this the path that you opened for me, Lord Yam-Nahar?*
There wasn't any answer from his own thoughts, nor was there from Lord Yam-Nahar.
He felt stupid, but something about this boy drew him in.

"What is your name?" Said Aditya in Indonesian.
The boy didn't answer, so Aditya repeated it again in English, "what is your name?"

"*Yang Gi-Hwan ibnida,*" replied the boy.

"Yang Gi-Hwan, don't worry okay," Aditya pointed at himself, "I am Aditya."

Yang Gi-Hwan hugged Aditya, his cries wetted Aditya's shirt, but Aditya didn't mind.
Based on the presence of Aurelia, and the fact that Yang Gi-Hwan was left alone, Aditya suspected that it was because of Yang Gi-Hwan's peculiarity.
Yang Gi-Hwan can summon spirits.

"I wasn't always a spirit, I used to be a real dolphin, roaming freely in the ocean.
I died, I got trapped in hell, and Gi-Hwan helped me out of hell," said Aurelius.

"You can read minds?"
Asked Aditya, Aurelia didn't respond.

## Anthony Matthias

A year later, no one claimed to have lost a son named Yang Gi-Hwan in their city.
No one from the neighboring cities, not even from the counties.
To the best of Aditya's knowledge, the boy had no parents.
Aurelia was no help either, she didn't cooperate in identifying Yang Gi-Hwan's parents.
She decided that Yang Gi-Hwan's parents weren't worthy of her attention, as they dumped Yang Gi-Hwan to die in the woods.

Debating on what to do with this Yang Gi-Hwan boy, Aditya found himself looking at the boy.
Yang Gi-Hwan didn't say anything, he just sat in his bed in silence.
Aditya sighed, *screw it, whether or not it is the right thing to do, I couldn't just leave this boy alone,* he thought.
"Yang Gi-Hwan," said Aditya, the boy turned his gaze to Aditya, "do you want me to be your new father?"

Yang Gi-Hwan smiled.
Aditya's heart warmed up, he smiled back.
Yang Gi-Hwan rose from his bed and ran to find Aditya's leg, he hugged it.

"No," said Yang Gi-Hwan, he looked up to Aditya, who kneeled down, "you're more like a *hyung*."

*Hyung* is used to refer to an older brother.
*Whatever the boy wants to call me, I am okay,* thought Aditya.
Aditya let his hands wrap around Yang Gi-Hwan.

"*Hyung* it is," he said.

Indonesian officials wouldn't let Aditya fill up the papers required to adopt Yang Gi-Hwan in *Hangul*, the Korean script.
Using Yang Gi-Hwan's romanized name would be okay, but the official suggested using a more Indonesian name, to help the boy blend in.
Aditya sighed, he wasn't in a mood to argue, and the officials weren't very wrong either.
Aditya's concern was that Yang Gi-Hwan might be bullied if he was to keep his Korean name.
The name sounded too chinese for the locals, that it might alienate Yang Gi-Hwan from his peers.

Aditya decided to give Yang Gi-Hwan a new name: Anthony Matthias.
It was out of convenience, and wasn't a particularly meaningful name either.
A combination of the first two names that came to his thought.

Anthony was happy to be given a new name.
Apparently he was eager to have an English name.
Anthony was more fluent in English than Aditya's Indonesian, despite being a Korean.
Aditya always thought that Korean tended to be choppy in their English.
Aditya's best guess would be that Anthony's parents were a mixed marriage couple, a marriage of two individuals with differing nationality.

Anthony was sleeping soundly at their bed, as Aditya looked at him.
In their house, they must've taught Anthony to speak in either English or Korean, thought Aditya.
For some reasons, his parents didn't bother to teach him Indonesian.

"Must be from a rich family," Aditya thought to himself, he left Anthony to rest.

## Tony, we're going home

"I'm sorry, Sir, we can't let Anthony continue his study here," said the Principal.
There were bruises on her face.

"But, why?"

"It's complicated. He did not comply with our teachings."

"He's a bright kid, you said it in the first few weeks he enrolled here."

"Sir, please, with all due respect, just take your son out of our school,"
the Principal's eyes were holding back tears, she showed Aditya the way out.

"If he's not smart enough, I could help him at home," pleaded Aditya.

"The problem is religious-"

"I'll make him attend churches-"

"JUST BRING THE DAMNED SON OF A DEVIL OUT!"

All remnants of a civilized manner had left the Principal's head, as she started to scream obsessively.
She cried, and the resident teachers barged in to help her.
Aditya was dismissed by force.

When Aditya approached Anthony, he realized why Anthony was expelled.
Anthony was talking with Aurelia.
A number of school workers conversed with audible whispers as they passed Anthony, they didn't seem to realize that Aditya was Anthony's legal guardian.

According to their whispers, that Aditya doubted to be considered whispering, Anthony was having a tantrum at school, when the dolphin ghost came out and wreaked havoc at Anthony's immediate vicinity.
What was more alarming, was that the Principal, who was also a lead Evangelist of the school's church, attempted an exorcism on Anthony.
It was evident, from the bruises on her face, that the exorcism didn't end well.

Aditya sighed, and approached Anthony.
"Tony, we're going home," Aditya smiled.

Anthony smiled back, and raced with enthusiasm toward Aditya.
Aditya held Anthony's small hands, and they walked away from the school premises.
Aditya was sure that the school was avenged properly with the damage Aurelia caused, for performing an exorcism without bothering to inform him as Anthony's legal guardian.

## Kids like Anthony

School after school rejected Anthony.
Some because Anthony was feared by his classmates, some because the teachers feared Anthony and his "demonic powers."
Some had the courage to blatantly say that Anthony was the son of Satan.

The rumor spread even to Aditya's immediate neighborhood.
They weren't even shy to speak about it in front of Aditya and Anthony.
Aditya was too tired to deal with them, after countless attempts to try to clear things out, that Anthony was a good kid, if they disregard the dolphin ghost that occasionally followed him around.
But what could Aditya explain about the presence of the dolphin ghost anyway?

Until one day, a bearded man knocked Aditya's door.
He knocked again, and again.
Aditya opened the door, and found a bearded man dressed in gray formal wear.
The wear caused a walkie talkie-like contraption pinned on his left shoulder to stand out.

"I am Arwin," said the bearded man, extending his arm toward Anthony, "Arwin Muerte."

Aditya took the hand, "Aditya Wijaya. May I help you?"

Arwin wasn't alone, a young boy in white shirt with indigo denim outerwear, perhaps fifteen years old, stood beside him.

"This is my son, Dominic Muerte," Arwin introduced, "like your son, he's gifted."

"What?"

Arwin nodded to Dominic.
Dominic advanced toward Aditya, and his hand raised to Aditya's chest.
A pressure could be felt on Aditya's chest, even before Dominic's hand reached him.
He was jolted backward at that instant and hit the floor with a thump.

"What happened?" Aditya asked.

"Dominic has telekinesis, the ability to move objects remotely," said Arwin.

"And how is it my business?"

"I'm here to represent our institution, the Extrasensory perception and Psychokinesis Laboratory, EPL for short. We study esoteric powers and help those that wish to improve their powers."

Aditya stood up, cleaning his back, and exchanged looks with Arwin.

"Our institution also runs a school, if that matters," continued Arwin.

"So your school wouldn't expel kids like Anthony?"

"Our institution is open for anyone, gifted or not. You included, if you like."

"I'm sorry, I graduated already."

"Vocational study is a thing you know.
Or you could join our community.
Any way you like it," Arwin smiled, "at least in our community, you don't need to carry the burden of raising a gifted child on your own."

## What about Anthony?

The school Arwin mentioned was set up like any normal school.
The only difference was the presence of certain extracurricular classes.
Disguised as spiritual and martial art classes, they appeared to be just ordinary extracurricular activities.
Arwin gave Aditya a tour while waiting for Anthony's class to finish.

"The ability to master the environment is what we are cultivating here.
That ability is classified into what we now call the Magic Spectrum.
Three of the most common would be Type Blue, Type Green, and Type Yellow," said Arwin.

Aditya looked into one of the classrooms.
A gymnastic class where the students were practicing.
Nothing weird, except to the fact that they practiced without touching anything.
Objects there were free-floating, while the students concentrated on those objects.

"Type Blue consisted of those with the ability to interact with tangible objects remotely, or colloquially called psychokinetic powers, sometimes telekinesis," said Arwin.

At the other room, Aditya observed a bunch of students flipping through papers supposedly containing the problems for them to solve, and they wrote answers at their worksheets.
However Aditya noticed something, the papers they're flipping through were blank sheets.

"Type Yellow consisted of those with extrasensory perceptions, usually passive skills.
The catch-all name of Type Yellow would be those with the sixth sense.
In this batch," Arwin pointed at the classroom Aditya was inspecting, "are those with the ability to observe the properties of objects not normally seen to the naked eyes.
For this particular session, we projected pictures to those papers before handing those papers to them, and those students were instructed to look at the history of interactions those papers had that contain specific images.
Then they have to write it down for scoring."

"They could do that?"

"About zero point one per mille of all masses on the surface of Earth are the mass of reflexium particles, a part of the pervasive and ubiquitous network that enabled magic-like activities to occur.
Type Yellows, can access sensory information that the reflexium particles managed to store, as long as the information is still there."

"So," Aditya looked around, "what about Anthony?"

Arwin smiled.
He brought Aditya to a meditation class, where Aditya could observe Anthony sitting in a chair, conversing with Aurelia.
Anthony was the only one in that class other than the instructor.

"A rarity they are," said Arwin, "we don't have many Type Greens.
Most new mages manifest their powers to be Type Yellow or Type Blue."

"What is so special about them?"

"Type Green is a mixture of Type Yellow and Type Blue, but distinctive from the other two.
They are able to interact with intangible objects.
The ability to converse and interact with spirits is one of the manifestations.
Others can alter your perception of reality by interacting with your essence, your soul.
Some can form pacts with powers," explained Arwin.

"So, Aurelia is a spirit?"

"You can say that.
Aurelia was a living being that then died and her soul continued on.
She was in hell, but Anthony helped her out.
It was a rare ability, even among Type Greens.
Anthony's kind of skill is often called Necromancy, the art of divination via contact with those that have died."

Aditya found himself led to yet another room, where all students had the same walkie-talkie-like device pinned to their shoulders, just like what Arwin had.
One of the instructors ordered one of them to lift a vase positioned at a table in the middle of the room.
One stood up, and they unpinned the device, turning it on and waved the antenna toward the vase.
The vase lifted off.

"Some of us, that could not perform any of those abilities, are provided with training wheels," Arwin unpinned the device at his shoulder, "this is a Computerized Organotronic with Neodymium-based Calculation Core, or a Conductor for short."

"That is not even a proper acronym...," said Aditya.

"We're still looking for a better name, so bear with it for now," Arwin's lips were tightened, "the point is, this device allows us to communicate with the reflexium particles like any natural mages.
It helps us to conduct the environment the way we see fit, though limited to what this Conductor can compute."

"So I will also be converted into a mage here, with or without a training wheel?"

"If you like to, I can procure a Conductor for you."

"Meh, being a mage is not my thing.
So what else can I do here, in this," Aditya raised two of his fingers on each hand, "...,community?"

"Be a part of our organization.
We have non-mage branches here as well.
Mastering magic is never our main objective, you know," said Arwin.

Arwin brought Aditya to a building.
It was a featureless, unremarkable building by the edge of the campus complex.
The reception clerks at the lobby appeared to be quite ordinary, not even very attractive.

Past the lobby and the ground floor, however, the building was bustling with activities.
People roamed about, documents literally flew from one section to another.
Arwin greeted and was greeted by a lot of them.

"Welcome to the control center of the Extrasensory perception and Psychokinesis Laboratory, Kendari City Branch," said Arwin, "where we change the world step by step, under the guidance of our patron god, Lord Yam-Nahar."

Aditya however, couldn't focus on any of those sayings.
His mind stopped working when Arwin mentioned Lord Yam-Nahar.
*Lord Yam-Nahar is the answer,* thought Aditya to himself, *he said he'd guide me.*

Arwin continued to speak about the aim of this organization.
An organization that works in the shadow with an aim to destroy the current governments of the world to establish a new world order.
A world of equal chances, free from poverty, free from scarcity of basic needs, no rulers needed.

*Is this your guide? Lord Yam-Nahar?* Thought Aditya.
It felt right, just like what he wanted to achieve.
Anarchism without competitions, with the aim for the benefits of all.
Personal freedom would be highly upheld, with a welfare system that ensures all basic needs of its citizens to be fulfilled.

"When magic was available for anyone to access, labor would no longer be required.
People would no longer need to focus on how to procure materials, on how to process them, on how to deliver them to where it would be needed.
People would then focus on their personal growth, of the limitless path of self-exploration and self-expression.
True freedom, where they would not be bound by struggles just to stay alive," said Arwin.

A liberal-left dream, with a concrete means to solve their problems: equal access to magic, not only restricted by those with the gift to naturally perform it.
Not only that, mingling with a magic organization might be a good start to obtain the esoteric request of Lord Yam-Nahar: to acquire Lord Yam-Nahar's *will*, whatever it means.
*That's it,* thought Aditya, *this is the answer.*

"I'm in," said Aditya.

Arwin's eyebrows were raised, then squinted.
His gaze took some moment to observe Aditya,
"that's it? You're in? I haven't even finished my explanation."

"Do you want me in or not?"

"I like your spirit," Arwin smiled, "of course I'd let you in."

## Have you read the Bible?

It was year two thousand and thirteen, Anthony was enrolled in a junior high school.
He was assigned to a normal school, to enable him to blend in into normal social circles.
It was when he learned an actual power struggle, the survival of the strongest.

It was the day a boy was harassed by a group of junior high seniors at the toilet.

"You can't run now, Kiel," said the head bully, shoving the fragile thin boy to the corner near an urinal, "now where's my money?"

Kiel didn't respond at all, his gaze was cold but stern, and it was aimed at the head bully.

"Do you think you're strong?" Said one of the accomplices, he gave Kiel a kick to his ribs.

Anthony raised his hand to one of the bullies from their back.
He was about to speak the name of Aurelia, when a warm but firm grasp lowered his arm.
The owner of that grasp was a high school student with a lean body and white hair -it wasn't gray, but white, as white as a plain paper, with a silvery quality.

"Hi, I am Michael Guntur," greeted the boy with white hair to the bullies.

The bullies, despite being about two years older than Anthony, had larger bodies compared to the boy with white hair.
They looked at Michael, scanning every inch of his body, and quickly dismissed him, proceeding to kick Kiel.

"You're Ezekiel Tanputra, a second-grader junior high student, right?"
Continued Michael.

The bullies stopped, they weren't sure what was happening.
"Hey kid, don't you see we're having a business with this sucker here?"
Said the head bully, pulling Kiel's necktie,
"go away and mind your own business."
The entire gang laughed.

Michael's hand, still grasping Anthony's wrist, didn't shake.
Anthony never thought anyone would be this calm, facing three boys bigger than him.
Grades didn't matter when it comes to a fight, a high school student would not be any stronger than a bunch of giant junior high school bullies.
Anthony would expect that Michael's eyes would at least waver at the threat.

That was when Anthony realized that Michael's irises were white.
That was when Anthony realized that there were no traces of reflexium particles in Michael's body.
That was when Anthony realized that Michael was not nobody, definitely wasn't anything he had ever encountered in his entire life.

"You're not doing business here.
You're bullying, and that isn't nice" said Michael again.

The sternness of Michael's tone somewhat caused the gaze of the bullies to quiver.
"G-go mind your own business!" said an accomplice with a kick thrusted toward Michael.

Effortlessly, Michael dodged it, and with his vacant feet, slid the thrusting foot away from the other, stretching the accomplice's feet as far away from each other.
The accomplice screamed in pain due to an excessive strain on his abductor muscles.
*He would be having a hard time trying to stand up,* thought Anthony.

He was in fact unable to move.
His abductor muscles were so in pain he couldn't move them without causing more pain.
He cried, and cried, while the head bully cursed him for not being helpful.

"You're not being nice," said Michael.
His gaze didn't move away from the head bully.

The remaining bullies backed away as Michael approached them.
There was something about being extremely calm under threats, that the ones threatening found it frightening.
Michael didn't need to be brawny or act to scare out the bullies, he just needed to be extremely calm, indifferent, and firm in his approach.

Normal people didn't do that.
Michael didn't show fear, and bullies feed off the fear of those they threatened.
With no fear to feed on, the fear emerged in the bullies themselves.

Disregarding the two frozen bullies, and one sobbing bully in a split position at one corner, Michael helped Ezekiel up.
Ezekiel, however, prostrated in front of Michael, "let me be your accomplice!"

Michael smiled, "I'd teach you, so you don't have to be anyone's accomplice."

"Then, let me be your student!" said Ezekiel again.

"Very well."

"L-let me be your student too," said Anthony.

Michael looked back to Anthony.
His gaze was an old soul, full of assurance and wisdom, that Anthony backed away unconsciously.

"Anthony Matthias, we're not supposed to meet yet," said Michael, "let us be in our own ways until the day we meet again."

Michael and Ezekiel left the bathroom.
The two bullies helped their sobbing friend to get up.
They too, left the toilet, all three of them sobbed.
Anthony was left in the toilet, petrified, not sure what was happening.

Weeks after, it appeared that the older brothers of the bullies confronted Michael for hurting their younger brothers.
Rumor had it that they became Michael's students afterwards.
More and more students joined Michael as his students, and they started to acquire members from different schools as well.

Whenever Anthony tried to look for the group, it was like trying to catch eels in a mud pool.
They were slippery and agile, always slipped away whenever they were in one's grasp.
It wasn't long until the new group gained a name, somewhat along the line of a killer whale, as they hunted in packs.
It was an Orca group, or so Anthony heard.

Anthony's attempt to get into the group bore no fruits.
Until one day, about four thousand defectors of the group led by Dominic Muerte came to the EPL.
Dominic Muerte, the son of Arwin, was a spy for the EPL, who managed to join Orca group by chance.

He revealed that Michael's teaching allows non-mages to acquire mage abilities without any aid.
Aditya, then was a newly appointed chief of the EPL, knew that they must acquire that teaching.
EPL must acquire the so-called Orca group.

"What is it like to be in the group?" Asked Anthony to Dominic.

"It was more like joining a *dojo*, we studied a form of martial art," said Dominic.

"Martial art? How does it relate to the acquisition of esoteric abilities?"

"Control over one's meta, it is divided into tiers.
Control over one's body comes first, then followed with metapresence, the ability to project yourself outside your physical body.
After that, would be metaforming, the ability to transform your meta projection to the environment."

"And that metaforming, is how *they* perform magic?"

Dominic scoffed, "*we* don't use the term magic.
It was an inherent part of nature that obeyed one's command only when one is faithful enough to command nature.
Have you read the Bible?
At Matthew chapter seventeen verse twenty."

Anthony shaked his head.

Dominic smiled, "it says that, if you have faith like a grain of mustard seed, you can order a mountain to move, and it will move."

"Just that?"

"The problem is in the faith.
Michael's teaching involves body and mind practices that would help in shaping our faith, in understanding the nature of ourselves, of our meta."

"I don't need to do that to perform magic, I just do it," bragged Anthony.

Dominic scrubbed Anthony's hair in delight, "of course, you're naturally gifted, but not everyone is.
To you, faith comes effortlessly.
To me as well."

Anthony tidied up his messed up hair, and Dominic relaxed at the couch.
His gaze wandered to the ceilings, "but this Michael guy, is *no shit*.
His methods were far more comprehensive, and intuitive compared to our usual study curriculum.
It allowed me to master a more delicate technique with a novel perspective of how to do it.
I could never have studied it here."

Aditya joined the table Anthony and Dominic were sitting on.

"Dominic," said Aditya to the defector, "come and join me, we have to discuss something."

Anthony came to learn that night that despite being very good at the so-called *metaforming*, Dominic was very bad at teaching it.
Among the four thousand defectors that joined Dominic, at various proficiency over the technique known as M.G. Style, none was as good as Dominic in mastery.
The only way to study the teaching, would be to force Michael himself to teach them.

Skirmishes of EPL officers and ex-Orca members were launched toward the Orca group.
It resulted with a series of bombing to various facilities associated with the target group.
One of them was a bombing at Anthony's school, which was also the headquarters of the Orca group.

"Is it really necessary, father?" Asked Anthony, he sat on their bed.

"Yes," said Aditya, slowly positioned himself beside Anthony, "we need the technique, so more people could use magic.
Only when magic is widely spread, could our goals be achieved."

Anthony nodded, but his gaze turned to his fingers, playing with each other.
He remembered Michael, the kind guy behind the entire Orca movement.
"Why couldn't we just ask them nicely?"

"We couldn't," Aditya sighed, "another mage organization was behind that group, which was why Dominic and his mates left the group in the first place."

"What group?"

"Dominic said, it was the *WTF* guys.
He just couldn't remember what it stood for."
