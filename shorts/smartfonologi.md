---
title: Smartfonologi
author: Hendrik Lie
date: 2019-04-17 01:21
tags:
---

# Smartfonologi

Angin meniup pepohonan di tengah malam, suara berderik dapat terdengar di tepian jalan. Suara gemeretak pepohonan terdengar seperti bisikan di ambang kesadaran. Pohon-pohon itu berdecit, berdecak, dan bergemercik. Sekelompok anak berusia belasan tahun menatap-natap sekeliling mereka, berusaha mencari sumber bisikan-bisikan malam tersebut. Anak-anak cewek berkerumun di tengah, di sekeliling Ibu Karina, guru mereka.  

"Anak-anak, tenanglah, di depan kita masih ada grup lain yang telah jalan duluan. Tidak ada yang harus dikhawatirkan" ucap Ibu Karina.

Nyaris tidak ada yang memperhatikan ucapan Ibu Karina, anak-anak cewek menerbitkan beberapa teriakan setelah mendengar bisikan-bisikan malam tersebut.

Di antara kerumunan tersebut adalah David, Ashton, dan Zean. Zean memegang erat lengan David dan Ashton, yang menarik mereka seperti tirai untuk menutupi dirinya ketika pohon-pohon mulai bersuara lagi.

"Oy, kenapa kau takut? Itu hanya suara-suara pepohonan saja," kata David, seraya menarik tangannya dari genggaman Zean.

Zean menggenggam lengan David lagi,

"Iya, mengetahui hal tersebut sama sekali tidak membantu –OH SUARA APA ITU!?"

David menggulingkan matanya ketika Zean menarik lengan Ashton dan David lagi.

"Tidak semua orang bisa bersikap datar seperti kita" komentar Ashton sambil tertawa.

"Teknisnya kau juga tidak sedang bersikap datar, Kakek," David memandang Ashton.

Ashton hanya tertawa, tapi Zean menanggapi hal tersebut sebagai kesempatan untuk bertanya. Tepatnya kesempatan baginya untuk melupakan lingkungan yang mengerikan ini.

"Kenapa kau selalu memanggil Asthon kakek? Dia sepupumu kan?"

"Oh tidak, tidak perlu kau pikirkan itu. Hal tersebut hanyalah hal yang sering kita lakukan dari dulu, dia merupakan peran kakek dan peran saudara dalam hidupku."

"Karena kau tidak cukup dewasa untuk disebut kakek, David," lanjut Ashton.

Zean baru saja akan melanjutkan pertanyaannya, namun perhatiannya terpecahkan dari teriakan Alex.

Di barisan terdepan, Alex memimpin jalan, Nicolas memegang senter dan mengarahkannya ke suatu tanda jalanan. Di sebelah kiri adalah jalan naik yang agak berbatu. Di sebelah kanan adalah jalan setapak dengan jurang di sisi kanan, tetapi jalanannya cenderung rata. Alex dan Nicolas berusaha membaca tanda tersebut, tetapi tulisannya telah lama tersapu hujan.

Ada dua masalah utama pada hari itu. Salah satunya adalah panitia pionir yang telah menelusuri lajur yang mereka lalui tidak melakukan laminasi maupun membungkus papan tanda jalan tersebut dengan plastik. Dan kertas yang ditempelkan ke papan tersebut akhirnya termakan embun pagi dan hujan. Tulisannya yang dicetak dengan font _cursive_ pun menjadi seluruhnya tidak terbaca.

Papan tersebut miring ke kanan, dan setelah beberapa waktu saling menatap, Alex dan Nicolas sepakat kalau tanda itu mungkin saja menunjukkan ke kanan, mengingat tanda-tanda sebelumnya semuanya menunjuk ke kanan.

"Kita ke kanan!"

Teriakan Alex nyaris saja tertutupi dengan bisikan angin malam. Hanya baris terdepan yang mendengarkan teriakannya, dan tanpa bertanya lagi mereka mengikuti arahan Alex dan Nicolas.

Tanpa diperhatikan oleh Alex dan Nicolas adalah ukiran tanda panah ke arah kiri di tanda petunjuk tersebut. Di sinilah masalah kedua mereka pada hari itu. Pada jalan yang salah, telah dipasangi tali di antara tiang penunjuk jalan dan sebuah tiang pancang di sisi lain oleh tim panitia pionir. Siapapun yang menancapkan tiang pancang tersebut, tidak menancapkannya cukup dalam.

Akibatnya, tiang pancang tersebut berjuang di bawah terpaan air hujan dan embusan kencang sang ibu pertiwi. Tanah tidak koperatif untuk ikut menopang tiang tersebut, terutama ketika air merayapi tubuh pertiwi, dan menggerus tanah dari akar tiang pancang tersebut. Pada akhirnya tiang pancang tersebut mencapai titik batasnya, ia harus menopang berat dirinya dan tali yang menjuntai di antaranya dan tiang penunjuk jalan, dan ia sudah tidak kuat lagi.

Tiang penunjuk jalan tersebut adalah saksi bisu tumbangnya teman seperjuangannya. Tak ada yang bisa dilakukannya, dan ia hanya bisa menangis hingga wajahnya luntur. Tampak rekan seperjuangannya dan tali yang menjuntai di antara mereka ditelan oleh aliran air lumpur yang merayapi tubuh ibu pertiwi.

David menyadari adanya tali yang mereka lewati, tanpa menyadari bahwa itu adalah sisa-sisa dari perjuangan heroik si tiang pancang. Ia tidak berpikir ulang mengenai hal tersebut. Ashton beberapa kali balik melihat ke belakang untuk mengamati tali tersebut, tetapi perhatiannya dialihkan oleh tarikan Zean untuk menutupi dirinya. Ashton yakin melihat kayu tali di ujung lain tali tersebut. Akhirnya perjuangan si tiang pancang pun lenyap bak tetesan air di telapak tangan yang menguap tertiup angin.

Keenan terus mengamati bintang di langit. Dia sangat menyukai segala hal tentang astronomi, dan mampu mengenali beberapa rasi bintang. Dia terus mengeluh mengenai awan-awan yang menutupi sebagian bintang-bintang tersebut.

"Jika langit cerah, pasti pemandangan di langit sangat bagus" komentarnya.

Apa yang tidak bisa dilihatnya bukan hanya bintang-bintang yang tertutup awan, tetapi juga akar pohon yang mencuat di atas tanah. Kakinya tersandung dan momentumnya menariknya ke tanah. Di saat tersebut, Ibu Karina segera menanggapi dan menahan Keenan, tetapi ia juga ikut tersandung dan jatuh ke jurang di sebelah kanan. Keenan berhasil bertahan di salah satu pepohonan saat ia terjatuh, tetapi Ibu Karina terjatuh cukup jauh. Segala hal tersebut terjadi dengan sangat cepat, dan seluruh grup panik, beberapa orang mulai berteriak. Alex dan Nicolas mencoba menggapai Keenan. David, Zean dan Ashton berusaha turun untuk menggapai Ibu Karina.

Saat mencapai Ibu Karina, David dan Zean cukup terkejut, telapak kaki Ibu Karina bergeser ke arah yang salah. Ibu Karina tampak kesakitan. Ashton mengamati kaki Ibu Karina dengan seksama,

"Saya rasa Ibu tidak bisa melanjutkan perjalanan, saya akan memanggil yang lain untuk membantu merawat ibu."

Ashton mendaki naik ke jalan setapak yang mereka lewati. David dan Zean hanya saling bertatapan, dan kemudian melirik ke Ibu Karina yang masih kesakitan.

Selang beberapa lama, beberapa anak telah berada di sekitar Ibu Karina, dan mereka menyiapkan tenda, kemudian mencoba untuk merawat kaki Ibu Karina.

"Kita harus segera kembali ke kamp dan memanggil bantuan," kata Alex.

"Iya, tapi sebaiknya kita membagi jadi dua grup, satu grup menjaga Ibu Karina, satu grup lainnya yang kembali ke kamp kita" lanjut Nicolas.

David yang ada di samping Ibu Karina mendengar bisikan dari Ibu Karina, "ingatlah kalian, apa yang pernah kita pelajari di pelajaran Geografi, dan Teknologi Informatika dan Komputer."

Suaranya lirih,

"David, Ashton, Zean, kalian pergilah ke kamp mencari bala bantuan, aku tahu kalian telah mempersiapkan seperti yang kita pelajari di kelas."

"Baik Ibu, tunggulah di sini, kami akan memanggil bantuan" balas Zean.

Ibu Karina hanya mengangguk, dan mereka bertiga mulai menanjak ke jalan tadi.

Alex melihat David, Zean dan Ashton menanjak naik, dan ia memanggil mereka.

"Tunggu, David, Zean, Ashton, kalian mau ke mana?"

"Ibu Karina meminta kami kembali ke kamp untuk memanggil bantuan" urai Zean.

"Ah, kalian penakut begitu, mana bisa bertahan di hutan begini. Nicolas, ayo kita bantu mereka."

David dan Ashton hanya memandangi Alex, Zean merespon,

"Ya, ada baiknya kita punya beberapa orang tambahan, iya kan David, Ashton?"

"Kami tidak meminta persetujuan kalian, ini kewajiban kami sebagai ketua dan wakil ketua kelompok," kata Alex lagi.

Nicolas mengikuti dari belakang.

"Ayo jalan sekarang," seru Alex.

"Oh tunggu," kata David, ia merogoh ponsel pintarnya, dan menekan beberapa tombol di antarmukanya.

"Hey, David! Fokus! Kita harus melihat tanda-tanda di jalanan!" teriak Alex.

"Tapi aku mau me-"

"Tidak, sekali tidak ya tidak. Tidak boleh perhatian kita teralihkan ketika kita mengemban tugas penting di sini!" potong Alex.

"Di sini juga tidak ada sinyal, tidak ada gunanya kau coba hubungi grup lain sekarang" lanjut Nicolas.

David mengeraskan genggaman ponselnya dan otot dahinya, giginya bergemertak dan pipinya memerah. Tenggorokannya bersiap untuk bersuara, mulutnya sudah terbuka.

Saat itu, tangan yang dikenalinya sebagai milik Ashton menepuk pundaknya. Ia mengalihkan pandangannya ke arah Ashton, yang menatapnya dan menggelengkan kepala.

David menghela nafasnya, dan tekanan di dahi dan genggamannya buyar.

"Saya rasa percuma berdebat dengan mereka," kata Ashton.

Zean, David, dan Ashton saling bertukar pemandangan untuk beberapa waktu, dan memutuskan untuk mengikuti keinginan Nicolas dan Alex.

"Oh, Keenan," kata Zean saat ia memandang ke belakang.

"Setidaknya biarkan aku ikut, aku yang menyebabkan Ibu Karina terluka."

Zean memandang ke David dan Ashton, tetapi Alex langsung berteriak dari depan,

"Ya, biarkan dia ikut. Dia perlu bertanggung jawab atas perbuatannya."

Mereka berjalan dan melewati beberapa persimpangan. David mulai menyadari ada beberapa tanda yang tidak bisa terbaca, dan tampaknya Alex dan Nicolas tidak yakin dengan jalan yang mereka ambil.

"Alex? Kau yakin ini jalan kembali ke kamp?" tanya David.

"Kita hanya perlu mengikuti jalan-jalan yang tadi kita lewati kan?" balas Alex.

Ashton menyadari Alex sedang menghindari pertanyaan David dengan pertanyaan lain,

"Kau ingat jalan kembali ke kamp?" potong Ashton.

Alex dan Nicolas hanya berpandangan.

"Tampaknya ini jalan yang tadi kita lewati?" kata Alex, Nicolas hanya mengangguk.

"Saya rasa seharusnya kita belok ke kiri di persimpangan sebelumnya," tambah Zean.

"Tidak," sambung Ashton, "kita bahkan seharusnya tidak tiba di persimpangan sebelumnya."

Alex dan Nicolas hanya terdiam untuk beberapa saat. Mereka saling bertukar pandang, dan Nicolas mulai membuka suara,

"Memangnya kalian sendiri ingat?"

"Ya, kita bisa menggunakan ponselku-"

David baru mau menyelesaikan kalimatnya ketika Alex berteriak,

"Tidak, jangan kau lanjutkan lagi. Kita sedang fokus mencari jalan kembali, jangan main ponsel!"

Kesal, David tidak membalas perkataan Alex.

***

Untuk beberapa saat, mereka berpikir keras apa yang akan mereka lakukan. Keenan mengangkat tangannya,

"Saya rasa kita harus mempertimbangkan arah mata angin dulu."

Alex dan Nicolas mengangguk,

"tapi bagaimana caranya?" tanya Alex.

"Kita bisa mulai dari mencari rasi salib selatan, di arah sana" kata Keenan tanpa merubah arah tangannya.

Ternyata ia bukan mengangkat tangan, tetapi menunjuk rasi salib selatan di dekat cakrawala.

"Oh, lalu dari salib selatan kita membuat garis imajiner, sampai menyentuh cakrawala?" sambung Zean.

"Tidak. Tidak beberapa jauh dari arah yang ditunjuk oleh salib tersebut adalah arah selatan."

Keenan mulai membuka telapak tangannya, dan berusaha melihat rasi tersebut dari frame yang dibuat oleh telapaknya,

"Tepatnya sekitar lima kali panjang salib tersebut, oh, selatan ke arah sana!"

Telapak tangannya membentuk pose menunjuk, David, Ashton, Zean, Alex dan Nicolas mengikuti arah telunjuk Keenan. Alex dan Nicolas mengangguk lagi.

Tidak ingin kalah, Zean lalu mencari bulan,

"Kita bisa konfirmasi dengan melihat ke arah bulan. Bulan sabit jika kedua ujung tanduknya disambung dengan garis imajiner, dan garis tersebut kita teruskan ke cakrawala, kita bisa menentukan arah kutub sebagai persimpangan antara cakrawala dan garis imajiner tersebut! Oh," Zean menghentikan kalimatnya ketika melihat bahwa sekarang sedang bulan purnama.

David, Ashton, Nicolas dan Alex mengembalikan tatapan mereka ke Keenan.

"Bagus, kita sudah tahu arah selatan, jadi kita bisa tahu arah utara, timur dan barat. Lalu?" tanya Nicolas, matanya berbinar dengan harapan dan wajahnya menjadi secerah bulan.

"Setelah itu kita harus tahu di arah mana kamp-nya, dan kita datang dari arah mana," lanjut Keenan, ia berhenti di tengah-tengah kalimatnya ketika ia menyadari bahwa ia tidak memperhatikan arah mata angin tadi.

Ia telah berfokus ke jalanan sejak ia terjatuh tadi.

Karena tidak mengetahui arah relatif kamp mereka, dan tidak mengetahui dari arah mana mereka datang, yang berarti ke arah mana Ibu Karina dan anggota grup lainnya berada, mengetahui arah mata angin menjadi tidak berguna, dan mereka tetap saja masih tersesat.

Keenan bisa saja memperkirakan posisi mereka dengan mengetahui waktu, arah mata angin, dan konstelasi bintang, jika dibantu dengan instrumen yang tepat. Walaupun mereka memiliki instrumen yang memadai, mendapati bujur dan lintang mereka pun tidak ada gunanya, mereka juga harus mengetahui bujur dan lintang posisi kamp mereka terlebih dahulu.

"Cukup, kita pakai saja ponselku" protes David.

"Itu tidak perlu!" protes Alex.

"Dan tidak berguna! Tidak ada sinyal di sini!" protes Nicolas, ia menunjukkan bar sinyal yang kosong di ponselnya.

"Kenapa kita tidak coba dengarkan David dulu? Memangnya kita punya alternatif lain?" protes Zean.

Saat mereka sedang berdebat mengenai ponsel David, Ashton berjalan mendekati Keenan,

"Kita perlu mempertimbangkan juga apa yang telah diajarkan oleh Ibu Karina sebelum-sebelumnya. Kau ingat apa yang diajari Ibu Karina?"

Keenan berpikir beberapa saat, dan ia memutuskan untuk bertanya,

"Hey guys, kalian ada ingat apa yang diajarkan oleh Ibu Karina? Tentang bagaimana kita mencari arah dalam situasi seperti ini? Saya cuman ingat yang menentukan arah mata angin, apa ada lagi yang ingat?"

Mereka semua terdiam untuk beberapa saat, diikuti dengan unjuk jari oleh David,

"Saya ingat" kata David.

"Kita mesti percaya David sekarang. Terbukti tidak ada satupun dari kita punya solusi lain kan?" sambung Zean.

Alex mengernyitkan alisnya, ia memiringkan kepalanya, dan matanya tampak seperti akan melompat keluar dari kepalanya,

"Tapi dia menyarankan hal yang tidak berguna, menggunakan ponsel pintar di tengah gunung begini! Ponsel pintar menjadi tidak pintar tanpa koneksi ke internet!"

"Apa kalian punya solusi lain?" tanya Keenan.

Alex dan Nicolas saling bertukar pandang lagi.

"Ibu Karina tadi sudah bilang ke kita sebelum kita berangkat: 'ingatlah kalian, apa yang pernah kita pelajari di pelajaran Geografi, dan Teknologi Informatika dan Informasi,' dan soal mempersiapkan sebelum memulai perjalanan," urai Ashton.

***

"Kalian tersesat, tidak tahu arahnya ke mana, dan kalian hanya memiliki sebuah ponsel pintar. Masalahnya adalah kau jauh dari peradaban, termasuk tower sinyal seluler. Bagaimana kalian bisa mengetahui arah dan lokasi kalian?"

Ibu Karina menyisir wajah-wajah di kelas tersebut, dan mendapati seseorang yang tampak ingin menjawab, tapi ragu untuk menjawab. Ia tampak menunggu untuk ditunjuk. Dan Ibu Karina menunjuknya,

"Ya, silahkan Zean!"

Zean bangkit dari kursinya. Pandangan dari seluruh teman-teman di kelas yang terbangun berpusat ke arahnya. Bukannya membuatnya malu, tatapan dari seluruh kelas tersebut menjadi bahan bakar rasa percaya dirinya,

"Di dalam ponsel pintar modern terdapat berbagai sensor-sensor, salah satunya adalah sensor magnet, yang bisa digunakan sebagai kompas."

Ibu Karina tersenyum,

"Bagus Zean,"

Seisi kelas bertepuk tangan. Alex dan Nicolas yang duduk di pojok belakang baru saja terbangun kaget dan ikut bertepuk tangan tanpa mengetahui alasannya.

Ibu Karina memberi kode untuk mengakhiri tepuk tangan, lalu melanjutkan pertanyaannya.

"Lalu setelah kau dapat arah mata angin, bagaimana kau bisa mengetahui posisimu?"

Zean tampak berpikir keras, ia memutuskan untuk duduk saja karena tidak mengetahui jawabannya.

Ibu Karina menyisiri wajah mereka satu persatu lagi. Alex dan Nicolas telah tertidur pulas lagi, ditutupi dengan buku dan ransel mereka yang sengaja diletakkan di meja.

Didapatinyalah seorang murid lagi yang berusaha mengalihkan pandangannya dari tatapan Ibu Karina.

"Ya, Keenan, silahkan menjawab."

Keenan melihat sekelilingnya, berharap ada Keenan lain di kelas. Tapi ia terpaksa bangkit ketika menyadari hanya dia satu-satunya Keenan di kelas tersebut. Untuk memastikan ingatannya tidak salah, ia menyempatkan diri bertanya,

"Sa, saya bu?"

"Iya, hanya kau Keenan di kelas ini."

Untuk saat itu ia jengkel mengetahui ingatannya benar.

"Jadi apa yang dapat kita lakukan setelah mengetahui arah mata angin dari ponsel kita, untuk mengetahui posisi kita?" ulang Ibu Karina.

Keenan tampak berpikir keras, tepatnya ia berjuang untuk menahan rasa malu yang dipupuk oleh tatapan dari teman-teman sekelasnya. Tidak seperti Zean, setiap tatap mata yang melihatnya menarik rasa percaya dirinya turun.

"Lihat tanggal dan waktu bu," jawabnya.

Ibu Karina tampak berpikir,

"Tanggal dan waktu?"

"Ya, tanggal dan waktu."

"Setelah itu?"

"Mengetahui tanggal dan waktu saat itu, kita bisa mengetahui dari konstelasi bintang jika di malam hari, atau posisi matahari di siang hari, digabungkan dengan informasi mata angin, untuk mengetahui posisi kita, dimanapun kita di permukaan Bumi bu!"

Rasa tidak percaya dirinya mendadak lenyap setelah berhasil menjawabnya. Ia bangga dengan jawaban yang diberikannya. Ibu Karina pun tampak mengangguk-angguk.

"Ya," kata Ibu Karina, ia masih memroses jawaban Keenan, "itu juga bisa, kerja bagus, Keenan" puji ibunya.

Seisi kelas bertepuk tangan lagi. Kali ini Alex dan Nicolas terlalu malas untuk ikut bertepuk tangan, mereka memilih memperbaiki posisi buku dan tas mereka untuk digunakan menutupi diri mereka yang sedang tidur.

"Ponsel zaman sekarang itu sangat canggih, tidak hanya bisa digunakan sebagai jam, kalender, kamera, kompas sekalipun, dan bahkan senter," lanjut Ibu Karina, "ia juga bisa digunakan untuk bernavigasi, bahkan di tempat yang tidak ada sinyal selulernya. Nah, bagaimana caranya?"

***

"Ingat, Ibu Karina pernah jelaskan," kata David, ia melirik Alex dan Nicolas, "GPS bisa bekerja tanpa jaringan selular, karena GPS menerima sinyal langsung dari satelit, saat tidak ada jaringan lain."

David mengeluarkan ponselnya, dan memperlihatkan posisi mereka di gunung tersebut. David menggunakan peta satelit.

"Tapi bagaimana kau bisa peroleh petanya? Tidak ada sinyal untuk mendownload peta tersebut kan?" lanjut Nicolas.

"..., _Offline Map_, Nicolas. _Offline Map_" kata Zean.

"Petanya sudah kudownload sebelum tadi kita mulai mendaki. Ibu Karina sering mengingatkan untuk bersiap-siap kan sebelum memulai sesuatu, kan?" balas David.

"Selain itu, ponsel pintar modern dilengkapi dengan sensor-sensor, seperti sensor akselerasi, magnet, elevasi, dan GPS. Pada saat seperti sekarang ini, kita perlu GPS dan sensor magnet," urai David.

"Untuk apa kita perlu sensor magnet?" tanya Alex.

"Kompas. Kita bisa menggunakannya sebagai kompas!" seru Zean.

"Betul, Zean. Kita bisa menggunakan sensor tersebut sebagai kompas" lanjut David.

"Tapi bagaimana caranya?" tanya Nicolas.

David tidak menjawab pertanyaan Nicolas, tetapi ia langsung membuka aplikasi kompas, dan menunjukkannya ke mereka. David melirik kompas tersebut, dan menunjuk ke arah selatan yang tadi ditunjuk oleh Keenan,

"Keenan, metodemu tadi cukup akurat. Arah selatan memang di sekitar arah sana" kata David.

"Seandainya saja sekarang bulan sabit, caraku juga cukup akurat," protes Zean.

Ashton menepuk pundak Zean,

"Ya, seandainya saja sekarang bulan sabit."

"Tapi masih saja itu tidak berguna," protes Alex.

"Karena kita juga harus mencari lokasi kamp-nya di peta, dan peta satelit seperti itu memperlihatkan keadaan seperti terakhir kali dipotret oleh satelit. Masalahnya adalah kamp kita baru dibuat hari ini, belum tentu juga satelit tersebut melewati gunung ini, dan walaupun dilewati, saya rasa foto di peta tidak diperbarui secepat itu," urai Nicolas, menyambung pernyataan Alex.

"Menarik, kau bisa mengetahui hal tersebut," kata Ashton, ia menepuk pundak Nicolas.

Nicolas tersenyum lebar, dagunya agak dinaikkan.

"Tapi masih ada saja kesesatan berpikir," lanjut Ashton.

Nicolas menurunkan senyumnya dan menatap Ashton, berusaha memahami apa maksud kalimat tersebut.

"Ingat mantera ini: bersiap sebelum memulai" lanjut Ashton lagi.

"Tadi sewaktu kita tiba di lokasi kamping, saya sudah menandai tempat tersebut di peta," urai David, ia memperlihatkan tanda bendera di petanya.

Nicolas mengangguk ringan,

"Lalu lokasi Ibu Karina?"

"Ya, benar, kita juga mesti tahu arah relatif kita ke lokasi Ibu Karina. Ini sama saja dengan masalah kita tadi, kita hanya mengetahui arah mata angin, tapi tanpa mengetahui arah ke lokasi Ibu Karina, mengetahui hal tersebut tidak terlalu berguna. Bagaimana nanti kita beritahu lokasi Ibu Karina walau kita sudah tiba di kamp?" tanya Keenan.

David memandang tajam Alex.

Zean menunjuk Alex,

"Itulah yang David lakukan ketika si Alex menegurnya,"

Alex memutuskan pandangannya dengan mata David, ia berusaha melihat sebatang dahan yang sedang berdansa di alunan angin malam.

"Jadi, um, pada akhirnya apa kau sempat menandainya di peta?" lanjut Keenan.

"Ya," jawab David.

***

Beberapa jam kemudian, sekelompok orang dengan seragam oranye dan ditemani oleh sejumlah senter yang cukup terang datang menjemput Ibu Karina dan anak-anak lain yang berjaga di dekat Ibu Karina. Mereka dibawa ke kamp, Ibu Karina ditandu sepanjang perjalanan.

"Ibu memiliki anak-anak yang cerdas, mereka bisa menemukan jalan pulang walau sempat tersesat di hutan." kata salah seorang petugas paramedis.

"Ya, saya memang mengajari mereka untuk memanfaatkan teknologi yang mereka gunakan," kata Ibu Karina.

"Maksud ibu?"

"Banyak anak-anak sekarang menggunakan ponsel mereka untuk hal-hal seperti menjelajah internet, atau mengambil foto. Tetapi banyak dari mereka lupa kalau ponsel mereka sebenarnya adalah perangkat sensor yang cukup canggih, dilengkapi dengan GPS dan komputer. Pengetahuan untuk memanfaatkan fitur-fitur tersebut seringkali tidak diajarkan di kelas, tapi aku ajarkan itu di kelasku. Lihatlah, pada akhirnya mereka bisa memanfaatkan teknologi untuk mencari jalan ke kamp kan?"

Paramedis tersebut hanya mengangguk-angguk kagum.

***

Keesokan harinya, Zean, Ashton, dan David berjalan melewati persimpangan sial tersebut. Perhatian Ashton dan David kini terpusat pada tali, dan tiang pancang yang setengah terkubur di tanah tersebut. Zean berusaha memahami tulisan yang telah nyaris luntur seluruhnya itu dari papan penunjuk jalan. Ia tidak dapat membaca tulisan tersebut, tetapi ia melihat ukiran tanda panah ke arah kiri, terlihat jelas di bawah sinar mentari pagi itu.

Ashton mengangkat tiang pancang tersebut, tubuhnya penuh luka-luka gores, kotor diselimuti lumpur. Setelah diterpa badai dalam usahanya untuk terus menegakkan diri dan menjaga agar tidak ada murid yang jalan ke arah yang salah, ia telah kalah jauh sebelum fungsinya dijalankan.

"Hey, tampaknya ini seharusnya jadi tanda pembatas agar kita tidak lewat ke jalan ini," kata Ashton setelah mengamati tali yang tersambung ke tiang pancang.

"Ya, kau benar. Jadi kejadian kemarin terjadi karena tiang ini telah jatuh duluan," jawab David.

"Tidakkah sebaiknya kita kembalikan seperti posisi semula?" tanya Zean.

Ashton mengangguk. Ia mengambil ancang-ancang, kemudian dengan segenap tenaganya dari tubuh mudanya yang berusia lima belas tahun dikalikan seribu tersebut untuk menikam bumi pertiwi. Tikaman tersebut seolah-olah memberi hidup baru bagi si tiang pancang, kini berdiri tegak dan dengan kokoh menompang tali pembatas di antaranya dan si tiang penunjuk jalan.

"Ternyata kau masih kuat juga ya, Kakek" ejek David.