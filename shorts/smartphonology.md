---
title: Smartphonology
author: Hendrik Lie
date: 2019-10-04
tags:
---

# SMARTPHONOLOGY

Breeze wind waived tree trunks in the middle of a night, shrieking cries of the bamboo branches could be heard by the side of a road. Wimpy whispers of the woods could barely be heard to a group of young conscious souls. A group of fifteen years old scoured the night warily in a narrow walkway, frightened by the weeps of the woods. Girls clustered in the middle, surrounding Miss Karina, their patron teacher. She was about twice their age that might, or might not make her the oldest of them all. On the outer boundary of the group were a similar number of equally frightened boys.

"Stay calm, kids, there are other groups in front of us. There’s nothing to worry at all," said Miss Karina.

Almost no one heard her assurance, the girls screamed after some shrieks of the night.

In between the crowd were David, Ashton, and Zean. Zean held the arms of David and Ashton tightly, providing him human shields against anything that might be present in the woods, natural or supernatural. Every shrieks he heard, he held their arms tighter in fright.

"Hey, why are you so scared? It was just the trees" said David, while trying to break his arm free of Zean’s grip.

Seconds after, his arm was on Zean’s grip again.

"Not helpful, David, not helpful. Knowing that doesn’t make it less scarier –WHAT WAS THAT!!??"

David could only rolled his eyes as Zean tried to cover himself behind David and Ashton.

"Not everyone can be as poker face as us, Dave" said Ashton, giggling.

"Technically, you’re not poker-facing, Grandpa," said David.

Ashton giggled, and Zean took the moment to ask them about Ashton’s nick: Grandpa. Or he was just trying to forget these frightening woods.

"Why are you calling Ashton Grandpa, David? I mean, I thought you are cousins or something, aren’t you?"

"Oh no, don’t mind about it. It was just our thing; he fits both the grandpa and brother role in my life."

"No, it was because you’re not old enough to bear the title of ‘grandpa’, David," continued Ashton.

Zean was about to continue his questioning, but his attention was drawn by Alex’s shouts.

On the foremost row, Alex led the way, and Nicolas held the lightbeam, aiming it to a roadsign. On the left was a steeping ascending and uneven road. On the right was a walkway right beside a cliff, but a considerably even road. Alex and Nicolas took their moment to interpret the sign, but the content of the sign had been long repainted by the rain.

There were two major problems at the day. The first is that the pioneering committee that scouted the route they’re in wasn’t laminating the roadsign, or at least wrap it under plastic sheets. And the paper printed writings glued to the sign was consumed by the morning dew and rain. The cursive printed notes was left unreadable under the elements.

The roadsign leaned toward right, and after some time gazing each other, Alex and Nicolas agreed that they were meant to take the right road, with consideration that on previous turns they took right roads.

"We are turning right!" screamed Alex.

It was almost unheard under the whispering breeze. Only the foremost row heard him and followed him to the right. The rest of the crowd followed unquestioningly: they put most of their attention to the frightening wails of the nature.

Unseen to them was an arrow marking on that road sign that aims left. And that was the second problem of the day. The wrong road had been marked by the pioneering committee, and a rope was used to close the road. The rope was hung as a fence between the road sign and a pole made of a tree trunk. Whoever erected the pole didn’t burrow its base deep enough.

As a result, the pole fought bravely under the blows of rainfall and torrential storm of the Mother Nature. The ground turned its support against the pole, as the water body creeps over the Mother Nature, carrying away the dirts from the base of the pole. In the end the brave pole reached its limit, it has to support itself and the rope between it and the road sign, and he could not continue the fight.

The road sign was the only silent witness of the bravery and the struggle of its comrade. Nothing could the road sign offer for help, and the road sign could only weep under the storm that its face faded. Its comrade and the rope between them swallowed by the mud body, crawled over the skin of Mother Nature.

David noticed a rope lying around the road they took, without realizing that it was the remnants of the heroic struggle of the pole. He did not pay enough attention to the rope. Ashton repeatedly tried to look back at the rope, but his attention was drawn by Zean’s pull to shield himself. Ashton was convinced that there was a piece of trunk attached to the other end of the rope. At last, the pole’s struggle vanished like the evaporating morning dew under the fiery presence of the morning sun.

Keenan constantly gazed at stars sparkling over the night sky. He loves astronomy, and can discern various constellations. At the time he complained about the clouds that covers the magnificent view of the heavenly firmament.

"Only if the sky is clear, the view would be spectacular," mumbled him.

What he didn’t see was not just the part of the sky covered by the clouds, but also a tree root protruding off the ground. It welcomed his foot first and granted his face a path to the ground. Miss Karina reacted fast enough to catch him before his face reached the ground, but she wasn’t fast enough to maintain both of their balance, and both of them fell over the cliff. Keenan managed to anchor himself to a tree as he fell, but Miss Karina wasn’t as fortunate. Everything flashed so fast that the entire group panicked: some of them screamed. Alex and Nicolas tried to approach Keenan, while David, Zean and Ashton scaled down to reach Miss Karina.

What they discovered was confusing at first. Miss Karina’s left foot was bent in a wrong way. She held the pain on her foot, ocassionally moaned her pain. Ashton examined the foot closely, his eyebrows winced.

"I don’t think she could continue the trip, I will get help" said Ashton.

He scaled up to the road, while David and Zean gazed at each other, and Miss Karina.

After some time, some kids managed to surround Miss Karina, and they established a tent, while the others tried to treat Miss Karina’s foot.

"We have to get back to the camp and call for help!" screams Alex.

"Yes, and I think it is best for us to split into two: one group to look after Miss Karina, and the other group to get back to the camp," continued Nicolas.

David that was right beside her, heard her whisper: "remember what we’ve learned on Geography, and Information Technology and Computer classes."

Her voice was weak.

"David, Ashton, Zean, you three should go to the camp and search for help; I know you’ve practiced everything I taught in my classes."

"Yes Ma’am, just wait here, we will get help," said Zean.

Miss Karina nodded slightly, and three of them scaled up back to the road.

Alex looked at David, Zean, and Ashton, and he called them over.

"Wait, David, Zean, Ashton, where are you going?"

"Miss Karina asked us to get back to the camp and get help" said Zean.

"Huh, you’re wimpy and wouldn’t be able to survive at the woods. Nicolas, let us aid them."

David and Ashton just gazed at Alex, Zean responded.

"Sure, there’s nothing wrong with a set of helping hands, isn’t it, David, Ashton?"

"We’re not asking for permission, it is our duty as a chief and vice chief of the team," said Alex.

Nicolas followed from behind.

"Let us go!" shouted Alex.

"Oh wait," interrupted David, he took his smartphone and manipulated the screen of its interface.

"David! Focus! We have to pay our attention to the road!" screamed Alex.

"But I was going to-"

"No is a no. We can’t be distracted while carrying over this noble mandate!" interrupted Alex.

"There’s no reception here, we both know that smartphone is useless without reception, not even useful to contact the other group!" followed Nicolas.

David’s grip to his smartphone tighten and his facial expression stiffen, his teeth grinded against each other, and his cheeks redden. His throat was about to voice, his mouth was open already.

At the time, a familiar palm reached his shoulder; he recognized the palm as Ashton’s. He turned his gaze to Ashton, that stared at him, and shaked his head.

David exhaled, stiffness of his face and his grip loosened.

"I don’t think there is any use to argue with them now," whispered Ashton.

Zean, David, and Aston exchanged glances for a few time and decided to follow their lead.

"Oh, Keenan," said Zean as he gazed to their back.

"At least let me join you guys, I was the cause of this whole incident."

Zean looked at David and Ashton, but Alex screamed from the front,

"Yes, let him in. He must pay for his doing!"

They walked through a number of crossings already. David realized that some signs are unreadable, and apparently Alex and Nicolas weren’t sure on the turns they took.

"Alex, are you sure that it is the right way back to the camp?" asked David.

"We just have to trace our path, don’t we?" replied Alex.

Aston realized that Alex was avoiding David’s question by asking another question,

"Do you remember the way back to our camp?" interrupted Ashton.

Alex and Nicolas exchanged glances.

"Apparently it is the path we took, isn’t it?" said Alex, Nicolas nodded.

"I don’t think we should take the left turn in the previous crossing," added Zean.

"No," continued Ashton, "we weren’t supposed to reach the previous crossing."

Alex and Nicolas stoned for a few quick seconds, they exchanged glances again.

Finally Nicolas spoke, "don’t you remember it?"

"Yes, we can use my smartph-"

David was going to complete his sentence when Alex screamed.

"No, don’t you dare to finish it! We are focussing at finding our way back! There’s no phone allowed!"

Irritated, David decided to say nothing.

***

For a few time, they thought hard on what to do. Keenan raised an arm,

"I think we should first determine the points of the compass."

Alex and Nicolas nodded.

"But how?" asked Alex.

"We can start by finding the Crux constellation, and there it is," said Keenan without changing the direction of his arm.

He wasn’t just raising his arm, but he was pointing at a constellation that resembles a cross near the horizon.

"Oh, and from the cross we can extend an imaginary line to the horizon isn’t it?" continued Zean.

"No. Just a few length away from the direction pointed by the Crux is the south pole."

Keenan extended his palm, and tried to frame the constellation with his palm.

"About five time its length, ah, that way!"

His index finger pointed at a direction that was then traced by David, Ashton, Zean, Alex, and Nicolas. Alex and Nicolas nodded again.

As if it was a challenge, Zean tried to demonstrate his knowledge on determining the points of the compass. His methodology differs slightly, with the use of another celestial body: The Moon.

"We can confirm that method by looking at  The Moon. If it was a crescent moon, we can connect both of its tips into an imaginary line and trace the shortest path of the line to the horizon. Then by that we can determine the pole on the intersection between the imaginary line and the horizon! Oh," his excitement dropped as he looked up at the full moon hanging on the sky.

"We can confirm that method by looking at  The Moon. If it was a crescent moon, we can connect both of its tips into an imaginary line and trace the shortest path of the line to the horizon. Then by that we can determine the pole on the intersection between the imaginary line and the horizon! Oh," his excitement dropped as he looked up at the full moon hanging on the sky.

David, Ashton, Nicolas, and Alex returned their gaze back to Keenan.

 "Good, we now know the direction of south; then we can discover the north, east, and west. And then?" asked Nicolas, his eyes appeared to glow the brightness of the moon.

After a certain moment of pause, Keenan continued, "afterwards, we have to know the direction of the camp, and where we came from…, ugh," he stopped after a realization that he had not been observing the directions of the compass from their point of origin.

He had been concentrating his attention to the ground ever since he tipped over the root, that caused this entire incident.

Without the knowledge of their relative direction to their camp, and at Miss Karina’s position, knowing the directions of the compass wouldn’t do much help, and they were still lost.

Keenan could’ve approximated their position by knowing time, the directions of the compass, and stargazing alone, with proper instruments. Even then, what they got would be their latitude and longitude, but not the camp’s or Miss Karina’s latitude and longitude as well.

"That’s enough, I’ll just use my smartphone," protested David.

"There’s no need!" protested Alex.

"And useless! There’s no goddamn reception here!" protested Nicolas, he demonstrated with his phone that its reception bar was zero.

"Why don’t we listen to David first? It is not like we have any other alternative," protested Zean in the end.

As they were debating about David’s phone, Ashton took a moment to walk by Keenan’s side, "we need to consider what had been taught by Miss Karina on her classes. Do you remember her lessons?"

Keenan thought hard, and decided to ask,

"Hey, guys, do any of you happen to remember Miss Karina’s lessons? About how we should find our ways at a situation like this? I just remember about determining the points of the compass, do any of you remember there rest of the lessons?"

Silence descended upon them for some moment, before David raised his finger,

"I do."

"I think we should trust David now. As seen, none of us has any alternative to offer anyway," continued Zean.

Alex winced his brows, he tilted his head, and his eyes were to jump off his head, "but his suggestion is useless, using a smartphone in the middle of a mountain trip! Smartphone is not smart without a connection to the Internet!"

"Do you have any alternative?" asked Keenan.

Alex and Nicolas exchanged glances again.

"Miss Karina told us prior to our departure: ‘remember everything I’ve taught on the geograpy course and the information technology and computer course,’ and about always prepare before starting a journey," explained Ashton.

***

"Suppose you’re lost, in the middle of nowhere, and all you have is a smartphone. The problem is that you’re as far away from the civilization as possible, including cell networks. What can you do to discern your direction and location?"

Miss Karina scans the faces of class participants, and discovered the face of a boy so eager to answer but didn’t. He was waiting to be pointed first, and so she did.

"Zean, you may answer."

Zean rose from his seat, and received confidence by the entire gaze invested at him. Instead of being embarrassed by the gazes, they fuel his fire of confidence.

"In our modern smartphone is a number of sensors, one of those sensors is a magnetic sensor, that we can use as a compass."

Miss Karina smiled, "Good job, Zean."

The whole class gave a congratulatory applause. Alex and Nicolas, who were seated at the back corner, were just been awakened by the thunderous claps of the awakened participants, followed suit and claps without knowing why.

Miss Katrina signed them to stop, and continued her question.

"Then, after you get the directions of the compass, how do you know your position?"

Zean thought hard, but in the end he decided to be seated back, as he couldn’t provide a viable answer to that.

Miss Katrina scanned the face of her class participants again. Alex and Nicolas was asleep again, covered by their books and backpacks, that are intentionally positioned at their desk.

She managed to capture a student that broke her eye contact the moment their eyes meet.

"Yes, Keenan, you may answer."

Keenan looked around him, wishing that there was another Keenan in the class. But he was forced to rise when he realize that he was the only Keenan at the class. To confirm, just in case his memory betrayed him, he asked.

"M-me?"

"Yes, you are the only Keenan in this class."

For a moment he hated his memory for not betraying him.

"So, what to do after knowing directions of the compass with our phone, to get our position?" repeated her.

Keenan thought hard, to be exact he tried hard to combat embarrassment fostered by the gazes of his classmates. Unlike Zean, every gaze poured at him dimmed his confidence.

"I’ll look at the phone’s date and time, Ma’am," answered Keenan.

Miss Karina was lost in thought for a perceptible moment,

"Date and time?" repeated her.

"Yes, date and time."

"And then?"

"Knowing date and time, combined with directions of the compass, we can get to know our position, anywhere on the surface of Earth, by looking at the constellations at night, or by the position of the sun at daylight, Ma’am!"

His confidence filled up instantly after he managed to formulate an answer.

Miss Katrina nodded slightly, "yes," she processed the answer for a little more moment, "that could work as well, good work Keenan," appraised her.

A thunderous torrent of claps poured once more for Keenan’s answer. This time, Alex and Nicolas were too lazy to clap, they decided that it was better to fix the position of their bags and books to cover their sleep time.

"Modern smartphone is so advanced, it can be used not just for a clock, a calendar, a camera, a compas, and even a flashlight," she paused, "it can also be used to navigate, even at a place without any cell network. Then, how?"

***

"Remember, Miss Karina taught us," said David, his eyes peeked at Alex and Nicolas, "that GPS can work without cell network, as GPS receives a direct transmission from the sattelites, when there are no other network."

David took his phone up, and showed their position at the mountain. David used a satellite image map.

"But, how could you obtain the map? There is no reception to download it, isn’t it?" continued Nicolas.

"…, Offline Map, Nicolas. Offline Map" said Zean.

"I had the map downloaded prior to our trip. Miss Katrina always reminded us to prepare before starting anything, didn’t she?" continued David.

"Also, our modern smartphone is equipped with sensors, such as acceleration, magnetic, elevation, and GPS sensors. At the time like now, we need GPS and a magnetic sensor," explained David.

"Why would we need a magnetic sensor?" asked Alex.

"Compass. We can use it as a compass!" exclaimed Zean.

"Yes, Zean. We can use magnetic sensor as a compass" continued David.

"But how?" interrupted Nicolas.

David didn’t answer his question directly, but by a demonstration: he opened a compass application, peeked at it, and pointed a south direction first pointed by Keenan.

"I must admit, Keenan, your method was quite accurate. South is that way" appraised David.

"If only a crescent moon is tonight, my method is just as accurate," protested Zean.

"Yes, if only a crescent moon is here," mocked Ashton, his hand was on Zean’s shoulder, giggling.

"Still that is useless," protested Alex.

"We still have to find the camp’s location from that map, and sattelite map showed us the terrain as captured the last time a satellite passed through above us. The problem is that our camp was established today, it is not like the satellite passed above us, and even if it did, I don’t think the map is updated that fast," explained Nicolas, continuing Alex’s statement.

"Interesting that you can postulate such thing," said Ashton, he patted Nicolas at his shoulder.

Nicolas smiled wide; his cheeks were almost consumed by the smile.

"But still a fallacy" continued Ashton.

Nicolas removed his smile and gazed at Ashton, attempting to comprehend what he meant.

"Remember this mantra: prepare before starting" continued Ashton.

"Prior to our trip off the camp, I marked the camp’s location on my map," explained David, he showed a flag at the map.

Nicolas nodded,

"And Miss Karina’s location?"

"Yes, we should be able to get our relative direction to Miss Karina’s location. That’s quite similar to our previous problem: if we just know the directions of the compass, without knowing Miss Karina’s relative direction, it would be useless. How do you think we tell people at the camp the direction of Miss Karina?" asked Keenan.

David gazed Alex sharp.

Zean pointed at Alex,

"That was what David attempted when Alex reprimanded him,"

Alex decided to break his eyecontact from David, he tried to gaze at a branch dancing on the tune of night breezes.

"So, um, did you manage to mark it on the map?" continued Keenan.

"Yes," answered David.

***

A few ours later, a group of persons with orange uniform accompanied with a number of bright flashlights picked Miss Karina and the rest of the kids that aided her. They were brought back safely to the camp.

"You have smart students, they managed to discover a way back despite being lost in the woods," said aone of the paramedic.

"Yes, I taught them to use technology at their disposal," said Miss Karina.

"What technology?"

"Plenty of kids nowadays use their smartphones for things such as browsing, or taking pictures. But most of them forget that their smartphones are a package full of advanced sensors, equipped with GPS and a computer. Knowledge to use that features were often overlooked and not appreciated. I included it to my curriculum and I taught them on my classes. Look, in the end they find their way back to the camp using only their phones, didn’t they?"

The paramedic nodded in awe.

***

The day after, Zean, Ashton, and David walked past the damned crossing, where all of last night’s incident started. Ashton and David had their attention pointed at a rope, and a pole half buried on theground. Zean tried to discern and read the faded printed text from the roadsign. He couldn’t read any single letter, but he managed to spot an arrow carving pointed to the left, under the morning sun.

Ashton raised the pole, its body covered with scratches, dirts, and dried muds. After facing the storm on its struggle to remain erect and to prevent anyone from going into a wrong road, it fell long before it could function.

"Hey, apparently it should be a fence so that we wouldn’t pick this road," said Ashton after observing the attached rope.

"Yes, you’re right. So last night’s incident occurred because this pole fell prior to our arrival," answered David.

"Don’t you think we should put it back the way it is supposed to be?" asked Zean.

Ashton nodded. He took a stance, and with all of his strength provided by his youthful body of age fifteen years times a thousand, he stabbed the Mother Earth. As the pole penetrated the Mother Earth, it is now erect firmly with a new life, to support the attached rope hung in between it and the roadsign.

"Surprisingly you’ve got some strength left, eh, Grandpa?" mocked David.

"Ternyata kau masih kuat juga ya, Kakek" ejek David.**