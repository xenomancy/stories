---
title: Four of Them
author: Hendrik Lie
date: 1 September 2021
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
---

<style>body {text-align: justify}</style>

A white tumbler, engraved on its surface in a gray monospace font was the word, SUNSTONE.
It was in the middle of the air, speeding to the ground.
A dent was made, following a huge bang of the impact.

Two men were looking at each other.
One had his eyes filled with anger.
The other, had his eyes painted with guilt.

Another man approached the angered man.
The angered man pushed the approaching man, it wasn't enough.
The approaching man hugged the man, pushing him away from the man with guilt.

A woman, dropped her ingredients to the floor, rushed to the man with guilt.
Tears formed on his eyes, he looked away.
His gaze was anywhere but the man he angered.
Her gaze caught his, and she gave him a hug.

Boys, that were quarelling over the counter, froze at the sight of that fight.
They looked at one another, unsure on how to react.
They did nothing in the end.

"Please, Steven, go with Stone," said the woman as Stone left the house.

"But, Lena," said Steven, his eyes alternated between Sun and Stone, " what about Sun?"

"I'll take care of him," said Lena, comforting Sun that was crying on her shoulder, "you go with Stone."

Steven didn't think twice, he rushed to Stone.

Lena gave a stern look to her children, and the boys ran to their room.

"Now, tell me, Sun, what happened?"

"I, I just want him to be happy," said Sun.

"By breaking up with Stone?"

"Tell me, Lena, is it wrong," Sun's voice started to break, "that I support him to get married?"

*Even though, you know he choose you already?*
She didn't say that.
She couldn't.
How could she?

Lena couldn't look away from Sun.
He was not too tall, nor was he too short.
His skin was tan, genetics, not the sun.

His gaze, that used to be full of life and joy, was dim.
His breath, that used to spread laughter and jokes, was sobbing.
His soul, then effervescent, was dark blue.

She couldn't find in him the soul she admired.
The soul, she used to love- still in love with.
Her own soul torn apart into her tears, and they melt in a howl.

"He always love kids," Sun started to regain his breath, his voice was still shaking, "he said if he ever had one, it would be a boy."

Something else lingered in her heart.
That one part of her that always ached whenever it found Sun and Stone having fun.
The part she buried so deep, no one ever noticed.
That part resurfaced, as it sniffed an opening, a chance.

"But it's impossible," said Lena to Sun.
*It doesn't feel right,* said Lena to herself.

"You," Lena paused, "you can't give him that."
*Shouldn't I help them to patch things up?*

There was a pause, as Sun gazed at Lena.
It was as if he couldn't believe what Lena said.
*It isn't wrong though,* thought Sun.

"Yes, I couldn't," continued Sun, he nodded, "but the woman he was set up with by his parents, could."

*But it doesn't matter, Stone choose you anyway, his happiness, is you, Sun. To Stone, it's always you,* was all she said in her head.
The other part of her, however, won the battle to her mouth, "I think, you did the right thing, to put his happiness first."

There was a pause, Lena was surprised.
Before Sun could catch her expression, the other part of her spoke to her.
*I did nothing wrong, I was there for my friend when he needed it the most.*

"You think so?" Sun said.

*I'm being a good friend to Sun,* said her other part, before it reached her vocal chord, "I'll support whatever decision you made."

There was a reluctant smile growing on Sun's face.
To Lena's eyes though, it was the smile that made up her sunshine.
"That's what friends are for, I'll be with you whatever you decided on."

Sun spread his arm, his chest welcomed Lena in.
When his hands held Lena in, butterflies fluttered in her.
It felt like she could reach his heart.
*Didn't I just telling him only what he wanted to hear, not what he actually needs*, said herself again.

The other part of her, spoke again: *anything that makes him happy, makes him closer to me, it's a win-win.*
It did feel like she was closing to his heart.
It felt like it, until it wasn't.

"Thank you, Lena," said Sun, his voice brightened, "thank you for understanding."

He dissolved the hug, smiling.
She put a smile to her face as well, as he rose to the ingredients she threw earlier.
"It's sad we couldn't use these young mango slices, they'd make a good spice."

"Y, yes it's sad," said Lena as she rose, "I'll get you more."

Sun's smile, was as bright as the sun, just like his name.
His heart, was as distant as it was from her.
The aches returned to her heart.

Instead of a beer, Stone picked a vanilla milkshake, with a vanilla ice cream as its topping.
He stabbed the ice cream topping intently as he spoke, "Sun. Is. Stupid. The. Stupidest. Man. Alive."

Steven felt sorry for the topping.
It didn't do anything wrong to Stone
Yet it had to sacrifice itself to Stone's wrath.

Steven's hot green tea cup decorated with latte art, was looking back at him, expecting to be sipped or stirred on.
They had to wait a little bit longer, as Steven's thought wandered on the fact that there were actual knives on his kitchen, and his wife was with Sun.
He was rest assured, knowing that Sun was not that kind of person.

The ice cream was still assaulted by Stone.

"Say, I think you owe me an explanation," said Steven.

"No I don't."

*He didn't, he didn't, right*, said Steven to himself, "but can't you at least tell me?"

Whatever remained of that poor ice cream topping could take a breath, as Stone changed his focus to Steven.

"How could he be so stupid, that he listened more to my parents," said Stone, "more than the man that loves him?"

Stone held the milkshake, his gaze returned to the topping.

"What did your parents say?" Said Steven, before Stone even put the spoon to the glass.

"My parents arranged a marriage," Stone put the spoon on the table, "they said I need to get married, before it's too late."

"But, you're with Sun, aren't you?"

"Yes, I am, or I was. Sun is stupid. I told him I would like a boy if I could have one, then he decided to break up with me," he took a spoonful of vanilla ice cream, "saying that it's my chance to get my own son."

Steven gazed at that spoonful of ice cream, as it disappeared behind Stone's lips, "don't you want a son?"

"What for?" Stone said, taking another spoon of ice cream, "I raised four of them," then another spoon, "four of us, raised four of your sons. As if it wasn't enough for me to experience parenthood."

"Right, right," murmurred Steven.
Long awaited by his green tea, he decided to take a sip of them.

"Then," Steven was disappointed as the hot green tea was no longer hot, "what is the problem?"

"Yeah, what's the problem?" Stone could no longer find any ice cream. He spooned them all out already, "oh right, the stupid Sun decided that I still want to have a son of my own!" Stone gulped a lot of the toppingless milkshake.

It was enough to make Steven feel full already, he put his cold green tea cup down, there was a pause, "why don't you tell him that?"

"What?"

"That you no longer need a son? That, you've raised four healthy sons, with us, already?"

"Stupid Sun," Stone scoffed, "he knew it already, he must be," his glass was empty at last, "I mean, we've been together for this long, he must've know right?"

There was a pause, as Stone looked at Steven's half empty cup, "are you not going to finish that?"

"The fact that Sun is doing whatever he is doing right now," said Steven, he pushed his cup to the side of Stone's glass, "might actually mean that he has no clue about it," the cup, then in Stone's hand, made it halfway to Stone's mouth, when Steven continued, "no clue at all."

Stone froze, "for real?"

"Have any better explanation?"

"That he's stupid?"

Steven took the cup back, and gulped it down, "that you're stupid!"
He put the empty cup down, Stone was still in the same pose.

"You didn't tell him anything, did you?" Steven still wished that the green tea was hot, while Stone shook his head, "then how do you expect him to know any of that?"

Both of the glass and the cup was empty.
Steven considered on buying another round, "when will you learn that communicating with codes, won't do anything good?"

Stone said nothing.

"See, if we want to order anything here, we must state our order. The server wouldn't be guessing our orders. They wouldn't, we would. We *tell* them exactly what we want."

Steven decided that they had enough, he signed the server to give them the bill, by making a writing gesture to the air.
The server nodded and rushed to the cashier.

"But you just give the server a code, and they underst-"

"Please," for the first time in many years, Stone was afraid of Steven's gaze, "don't steal my thunder, it wasn't the point."

"Right, right," murmured Stone.

After Steven made the payment, they were on their way to the parking lot.
Stone was silent, and Steven stopped.
He held Stone's firm shoulders, their gaze met each other.

"So, are you going to tell Sun or not?"

"I, I," Stone's voice broke down, his tears rolled.

"Are you?"

"I will, I will!"

"Yes, yes that's it," Steven's smile grew, "here."

Stone's firm hands wrapped around Steven.
Despite his stone-like stature, Stone cried like a boy.
Steven gave him a pat to his back, "there, there, you know what to do, don't you? What a good boy you are."

"You know I'd kiss you if not because of Sun, do you?" Stone said, as he wiped his tears.

"No, thank you, this lips is for Lena only," they both bursted in giggles.

The tumbler, despite having a dent and a scratch on its base, was cleaned and filled with vanilla milkshake by Sun.
Sun handed it to Stone.

Stone inspected the tumbler as Sun was speaking, "listen, I still hope that we can still be friends you know. I know that you're mad, but know this, I do this because I love you. You might not understand it now, but you will later."

"Stupid Sun," said Stone.

"I'm sorry?"

"I said, Stupid Sun! I choose you, dumbass."

"But, I couldn't give you a son, listen, I-"

"No, you listen to me. Four of us, raised four sons together, and you still think I need another son?"

"I-"

"You're either an idiot or crazy, to think I'd need another one. What I need is you, *us*."

Sun didn't say a word, Stone wrapped him in his arms.

"I don't need more, I just need you now."

Sun hugged Stone back.

It ached intensely in Lena's stomach.
Actually, Lena wasn't sure which ached more, her stomach, or her shoulder, that Steven shook hard, "look, I did it, *we* did it, we help them back together!"

"Yes, we did," said Lena, her other self said to her: *my attempts are fruitless.*

"I think it's a cause of celebration," Steven's gaze were fiery, "should we head to the bed?"

*I can't look at them together,* she said to herself, "yes, sure, let's go to the bed."
Her stomachache throbbed more.
It felt like she was being pulled from inside.
Pulled to an endless pit of pain, as she said to her other part, *it's impossible, he can't give me that.*

*What can't he give me?*

*His heart,* she said in her heart, *is just for Stone.*

The other part of her said nothing.

She was almost crying, but Steven came out from the shower room.
With his robe, he approached Lena, giggling.
The robe fell down, Steven looked intently at Lena, "shall we?"

"What say you?" Lena forced a giggle, *Stop Steven, I don't deserve you.*

Steven advanced.
She couldn’t think a word, her mind was blank, as they melt into one.
She could no longer feel the bed, not even the bedroom.
The delienation between her body and Steven's was gone.
She felt like in a wave, a warm current, and glittering.
Twitching, twirling, turning.

She found her inner voice chanted, *I am an animal, I want to have everything.*

*I want to have this carnal pleasure.*

*I want to do it with Sun.*

That night was supposed to be fun.
Lena looked at Steven, fell asleep with a smile on his face.
Hers, however, was dressed with guilt.
She ran her fingers across Steven's jawbone.
Smooth, firm, and pleasing.
Her heart, however, ached.

"Why, why do I have my heart on someone else that’s already been taken," she whispered, "not to this man here, the man that love me with all of his heart?"

Steven stretched, and Helena turned silent.
He was half awake, "what are you doing?"

"I, I need to use the toilet," Lena rose and left for the bathroom.

"I love you," said Steven.

She got a glimpse of Steven's smile, as he returned to sleep.
All that left, was her, and the other part of her.

"I love you too," said Lena.

*Did I mean it?*
